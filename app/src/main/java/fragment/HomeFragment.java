package fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.voalearningenglish.net.R;

import java.util.ArrayList;

import indicator.TabPageIndicator;

/**
 * Created by sang on 6/18/2015.
 */
public class HomeFragment extends BaseFragment {


    private static final String TAG = HomeFragment.class.getSimpleName();
    private static final String KEY_POSITION = "position";
    private ViewPager viewPager;
    private TabPageIndicator tabs;
    private ArrayList<String> listString;


    public static Fragment getNewInstance(int position) {
        Fragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void initUI(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabs = (TabPageIndicator) view.findViewById(R.id.titleTabIndicator);
        listString = new ArrayList<String>();
        listString.add(getString(R.string.video));
        listString.add(getString(R.string.article));
        FragmentPagerAdapter pagerAdapter = new CommonPagerAdapter(getChildFragmentManager());
//        tabs.set(getResources().getColor(R.color.white_light));
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);

        tabs.setViewPager(viewPager);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initUI(rootView);
        return rootView;
    }


    private class CommonPagerAdapter extends FragmentPagerAdapter {
        public CommonPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listString.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = ChanelListFragment.getNewInstance(0);
                    break;
                case 1:
                    fragment = RSSFeedCategoryFragment.getNewInstance(0);
                    break;
                default:
                    break;
            }
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }

        @Override
        public int getCount() {
            return listString.size();
        }
    }


}
