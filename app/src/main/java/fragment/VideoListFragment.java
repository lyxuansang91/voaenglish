package fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.androidteam.englishlearning.activity.EnglishApplication;
import com.voalearningenglish.net.R;

import java.util.ArrayList;
import java.util.List;

import adapter.VideoAdapter;
import configuration.APIConfig;
import de.greenrobot.event.EventBus;
import event.VideoItemClickEvent;
import logcat.CLog;
import result.playlistitem.Items;
import result.playlistitem.PlayListItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Define;
import utils.MakeToastMessage;

/**
 * Created by Admin on 17/4/2015.
 */
public class VideoListFragment extends BaseFragment {

    private static final String TAG = VideoListFragment.class.getSimpleName();

    private boolean TEST = true;
    private String playListId;
    private int count = 0;

    private List<Items> videoList = new ArrayList<Items>();
    private ListView lvVideo;
    private VideoAdapter adapter;

    private ProgressBar progressBar;
    private EnglishApplication application;

    public static Fragment getNewInstance(String playListId) {
        Fragment fragment = new VideoListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Define.KEY_SEND_PLAYLIST_ID, playListId);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_videos, container, false);
        application = (EnglishApplication) view.getContext().getApplicationContext();
        init(view);

        return view;
    }

    private void init(View view) {
        // bind view
        lvVideo = (ListView) view.findViewById(R.id.lv_video);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        // set adapter
        adapter = new VideoAdapter(R.layout.item_video, videoList, mFragmentActivity);
        lvVideo.setAdapter(adapter);
        lvVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Items items = videoList.get(i);
                CLog.d(TAG, "video name: " + items.getSnippet().getChannelTitle());
                CLog.d(TAG, "video id: " + items.getContentDetails().getVideoId());
//        CLog.d(TAG, "video image:" + items.getSnippet().getThumbnails().getDefault().getUrl());
                EventBus.getDefault().post(new VideoItemClickEvent(items));
            }
        });

        if (getArguments() == null) {
            MakeToastMessage.showMessageInShortTime(getActivity(), "Error.");
        } else {
            Bundle bundle = getArguments();
            playListId = bundle.getString(Define.KEY_SEND_PLAYLIST_ID);

            progressBar.setVisibility(View.VISIBLE);
            loadVideoList(playListId, "");
        }
    }

    private void loadVideoList(final String playListId, String token) {
        Call<PlayListItem> call = application.apiService.getPlaylistVideoList(APIConfig.API_KEY,playListId,token);
        call.enqueue(new Callback<PlayListItem>() {
            @Override
            public void onResponse(Call<PlayListItem> call, Response<PlayListItem> response) {
                if (response.body() != null && response.body().getItems() != null) {
                    for (Items item : response.body().getItems()) {
                        videoList.add(item);
                        adapter.notifyDataSetChanged();
                    }
                    if (response.body().getNextPageToken() != null) {
                        String token = response.body().getNextPageToken();
                        loadVideoList(playListId, token);
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                } else {
                    MakeToastMessage.showMessageInShortTime(getActivity(), "Error while loading");
                }
            }

            @Override
            public void onFailure(Call<PlayListItem> call, Throwable t) {
                MakeToastMessage.showMessageInShortTime(getActivity(), "Error while loading");
            }
        });
    }
}
