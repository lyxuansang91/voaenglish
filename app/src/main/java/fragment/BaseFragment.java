package fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created by sang on 30/01/2015.
 */
public abstract class BaseFragment extends Fragment {

    protected FragmentActivity mFragmentActivity;
    protected Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mFragmentActivity = (FragmentActivity) activity;
        mContext = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentActivity = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
