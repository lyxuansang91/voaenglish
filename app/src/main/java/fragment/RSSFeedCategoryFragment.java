package fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.voalearningenglish.net.R;

import java.util.ArrayList;
import java.util.List;

import adapter.FeedCategoryAdapter;
import de.greenrobot.event.EventBus;
import event.RSSFeedTypeClickEvent;
import utils.RSSFeedURL;
import utils.RSSFeedURLUtils;

/**
 * Created by HoangAnh on 10/06/2015.
 */
public class RSSFeedCategoryFragment extends BaseFragment implements AdapterView.OnItemClickListener {


    private static final String TAG = RSSFeedCategoryFragment.class.getSimpleName();
    private static final String KEY_POSITION = "position";
    ListView lvFeedCategory;
    List<RSSFeedURL> feeds;
    FeedCategoryAdapter adapter;


    public static Fragment getNewInstance(int pos) {
        Fragment fragment = new RSSFeedCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_POSITION, pos);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.feed_category_fragment, container, false);
        init(view);

        return view;
    }

    private void init(View view) {
        //binding view
        lvFeedCategory = (ListView) view.findViewById(R.id.lv_feed_category);

        feeds = RSSFeedURLUtils.getAllFeed();
        adapter = new FeedCategoryAdapter(view.getContext(),feeds);
        lvFeedCategory.setAdapter(adapter);

        lvFeedCategory.setOnItemClickListener(this);


    }


    private List<String> getFeedsName(List<RSSFeedURL> list) {
        List<String> rs;

        rs = new ArrayList<String>();
        for (RSSFeedURL type : list)
            rs.add(type.name);

        return rs;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RSSFeedURL rssFeedURL = feeds.get(position);
        EventBus.getDefault().post(new RSSFeedTypeClickEvent(rssFeedURL));
    }
}
