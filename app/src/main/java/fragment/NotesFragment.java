package fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.voalearningenglish.net.R;
import com.androidteam.englishlearning.activity.VideoDetailActivity;

import java.util.ArrayList;
import java.util.List;

import adapter.NoteAdapter;
import adapter.VideoAdapter;
import models.Note;
import result.playlistitem.Items;
import utils.Define;
import utils.NotesDatabaseHandler;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotesFragment extends BaseFragment implements ListView.OnItemClickListener{


    private static final String KEY_POSITION = "position";
    private ListView lvVideo;
    private ProgressBar progressBar;
    private ArrayList<Note> noteList = new ArrayList<Note>();
    private NoteAdapter adapter;

    public NotesFragment() {
        // Required empty public constructor
    }

    public static Fragment getnewInstance(int position) {
        Fragment fragment = new NotesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_videos, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View view) {
        lvVideo = (ListView) view.findViewById(R.id.lv_video);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        loadNotesList();
    }

    private void loadNotesList() {
        NotesDatabaseHandler db = new NotesDatabaseHandler(mFragmentActivity);
        noteList = db.getAllNotes();
        adapter = new NoteAdapter(R.layout.item_video, noteList, mFragmentActivity);
        lvVideo.setAdapter(adapter);
        lvVideo.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Note note = (Note) adapterView.getItemAtPosition(i);
        if(note != null) {
            Intent intent = new Intent(mFragmentActivity, VideoDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Define.KEY_SEND_VIDEO_ID, note.getVideo_id());
            bundle.putString(Define.KEY_SEND_VIDEO_NAME, note.getVideo_name());
            bundle.putString(Define.KEY_SEND_VIDEO_IMAGE, note.getVideo_image());
            intent.putExtras(bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
