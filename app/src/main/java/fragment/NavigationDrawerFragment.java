package fragment;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.voalearningenglish.net.R;
import com.androidteam.englishlearning.activity.EnglishApplication;

import java.util.ArrayList;

import adapter.DrawerListAdapter;
import de.greenrobot.event.EventBus;
import event.DrawListPositionChangedEvent;
import event.DrawerListItemClickEvent;
import logcat.CLog;
import models.CategoryDrawerListItem;
import models.DrawerListItem;
import models.HeaderDrawerListItem;
import models.MenuDrawerListItem;


public class NavigationDrawerFragment extends BaseFragment {

    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String TAG = NavigationDrawerFragment.class.getSimpleName();

    LinearLayoutManager mLayoutManager;
    DrawerListAdapter mAdapter;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    private ArrayList<DrawerListItem> mDrawerListItems;
    private RecyclerView rvDrawerList;

    private int mCurrentSelectedPosition = 0;


    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }


    public void onEvent(DrawerListItemClickEvent event) {

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (event.position != -1)
            EnglishApplication.getInstance().currentPosition = event.position;

        mAdapter.notifyDataSetChanged();
    }


    private void initDrawerList() {
        mDrawerListItems = new ArrayList<DrawerListItem>();
        DrawerListItem drawerListItem;
//        String access_token = EnglishApplication.getInstance().sharedPreferences.getString(vn.vivas.vgame.configuration.Configuration.ACCESS_TOKEN, "");
//        boolean isLogin = (!access_token.isEmpty());
//        if (!isLogin) {

        drawerListItem = new HeaderDrawerListItem(getString(R.string.app_name));
        mDrawerListItems.add(drawerListItem);
        //Home
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_menu_home, getString(R.string.homepage), DrawerListItem.ID_HOME);
        mDrawerListItems.add(drawerListItem);

        //Notes
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_note_list, getString(R.string.notes), DrawerListItem.ID_NOTE);
        mDrawerListItems.add(drawerListItem);
        //Favorite list
//        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_menu_manage, getString(R.string.favorite), DrawerListItem.ID_FAVORITE_LIST);
//        mDrawerListItems.add(drawerListItem);
        //Other apps
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_other, getString(R.string.other_app), DrawerListItem.ID_OTHER_APP);
        mDrawerListItems.add(drawerListItem);
        //Fan page
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_fanpage, getString(R.string.fan_page), DrawerListItem.ID_FAN_PAGE);
        mDrawerListItems.add(drawerListItem);
        //rating for us
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_rating, getString(R.string.rating_for_us), DrawerListItem.ID_RATING_FOR_US);
        mDrawerListItems.add(drawerListItem);
        //share application
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_share, getString(R.string.share_application), DrawerListItem.ID_SHARE_APP);
        mDrawerListItems.add(drawerListItem);
        //about us
        drawerListItem = new MenuDrawerListItem(R.mipmap.ic_about, getString(R.string.about_us), DrawerListItem.ID_ABOUT);
        mDrawerListItems.add(drawerListItem);
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        rvDrawerList = (RecyclerView) rootView.findViewById(R.id.rvDrawerList);
        rvDrawerList.setHasFixedSize(true);
        initDrawerList();
        mAdapter = new DrawerListAdapter(mFragmentActivity, mDrawerListItems);
        mLayoutManager = new LinearLayoutManager(mFragmentActivity);
        rvDrawerList.setLayoutManager(mLayoutManager);
        rvDrawerList.setAdapter(mAdapter);
        rvDrawerList.setItemAnimator(new DefaultItemAnimator());
        return rootView;
    }


    public void onEvent(DrawListPositionChangedEvent event) {
        mAdapter.notifyDataSetChanged();
    }


    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = mFragmentActivity.findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),
                mDrawerLayout,
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                mFragmentActivity.supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }
                showGlobalContextActionBar();

                mFragmentActivity.supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };


        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }


    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
//            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

//        if (item.getItemId() == R.id.action_example) {
//            Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(R.string.app_name);
    }
    private ActionBar getActionBar() {
        return ((ActionBarActivity) mFragmentActivity).getSupportActionBar();
    }
}
