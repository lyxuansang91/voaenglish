package fragment;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.voalearningenglish.net.R;

import java.util.ArrayList;

import adapter.ChannelAdapter;
import models.CategoryDrawerListItem;

/**
 * Created by Admin on 15/4/2015.
 */
public class ChanelListFragment extends BaseFragment  {
    final String TAG = ChanelListFragment.class.getSimpleName();

    private static final String KEY_POSITION = "position";

    private ProgressBar progressBar;
    private ChannelAdapter adapter;
    private RecyclerView rvChannel;
    private ArrayList<CategoryDrawerListItem> listItems;

    public static Fragment getNewInstance(int position) {
        Fragment fragment = new ChanelListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    private ArrayList<CategoryDrawerListItem> getChannelList() {
        TypedArray icons = getResources().obtainTypedArray(R.array.voa_playlist_drawable);
        String[] navCategoryItems = getResources().getStringArray(R.array.voa_channel);
        String[] navCategoryPlaylist = getResources().getStringArray(R.array.voa_playlist_id);
        ArrayList<CategoryDrawerListItem> lstResult = new ArrayList<CategoryDrawerListItem>();
        for(int i = 0; i < navCategoryItems.length; i++) {
            CategoryDrawerListItem drawerListItem = new CategoryDrawerListItem(icons.getResourceId(i, -1), navCategoryItems[i], navCategoryPlaylist[i]);
            lstResult.add(drawerListItem);
        }
        return lstResult;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chanel, container, false);
        init(rootView);
//        initData();
        return rootView;
    }

    private void init(View v) {
//        //ads
//        AdView mAdView = (AdView) v.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);


        //bind view
        progressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        rvChannel = (RecyclerView) v.findViewById(R.id.rvChannel);
        listItems = getChannelList();
        adapter = new ChannelAdapter(mFragmentActivity, listItems);
        rvChannel.setAdapter(adapter);
        LinearLayoutManager linearLayout = new LinearLayoutManager(mFragmentActivity);
        rvChannel.setLayoutManager(linearLayout);
        rvChannel.setHasFixedSize(true);
    }
}



