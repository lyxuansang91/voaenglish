package fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.androidteam.englishlearning.activity.EnglishApplication;
import com.voalearningenglish.net.R;

import java.util.ArrayList;
import java.util.List;

import adapter.FavoritePlayListAdapter;
import configuration.APIConfig;
import database.FavoritePlaylistDAO;
import de.greenrobot.event.EventBus;
import event.PlaylistItemClickEvent;
import models.Chanel;
import models.EChanelType;
import result.playlistinfo.PlayListInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.GetChannelList;
import utils.MakeToastMessage;

/**
 * Created by Admin on 15/4/2015.
 */
public class FavoritePlaylistListFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private static final String TAG = FavoritePlaylistListFragment.class.getSimpleName();

    private static final String KEY_POSITION = "position";
    private EnglishApplication application;


    public static Fragment getNewInstance(int position) {
        Fragment fragment = new FavoritePlaylistListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    private int count;


    private ListView lvChanelList;
    private FavoritePlayListAdapter adapter;

    private List<Chanel> listChanel;
    private List<String> listId;
    private List<PlayListInfo> list;

    private ProgressBar progressBar;
    private FavoritePlaylistDAO dao;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_playlists, container, false);
        application = (EnglishApplication) view.getContext().getApplicationContext();
        init(view);


        return view;
    }

    private void init(View v) {
        //bind view
        lvChanelList = (ListView) v.findViewById(R.id.lv_chanel);
        progressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        // set adapter
        list = new ArrayList<PlayListInfo>();
        listChanel = new ArrayList<>();

        count = 0;

        //get favorite list
        dao = new FavoritePlaylistDAO(v.getContext());
        dao.open();
        listId = dao.findAll();
        dao.close();

        Log.d(TAG,"favorite playlist: "+list.size());

        if(!listId.isEmpty())
        loadListChannel();

        for (String id : listId) {
            for (Chanel c : GetChannelList.getListChannel(EChanelType.ALL)) {
                if (c.id.equals(id)) {
                    listChanel.add(c);
                    break;
                }
            }
        }

        adapter = new FavoritePlayListAdapter(R.layout.item_favorite_playlist, list, getActivity(), listChanel);
        lvChanelList.setAdapter(adapter);
        lvChanelList.setOnItemClickListener(this);
    }

    private void loadListChannel() {
        //start loading
        progressBar.setVisibility(View.VISIBLE);
        for (String id : listId) {
            count++;
            Call<PlayListInfo> call = application.apiService.getPlaylistDetail(APIConfig.API_KEY, id);
            call.enqueue(new Callback<PlayListInfo>() {
                @Override
                public void onResponse(Call<PlayListInfo> call, Response<PlayListInfo> response) {
                    if (response.body() != null) {
                        list.add(response.body());
                        adapter.notifyDataSetChanged();
                    } else {
                        MakeToastMessage.showMessageInShortTime(getActivity(), "Can't load full data from internet.");
                    }
                    if (list.size() == count) {
                        progressBar.setVisibility(View.GONE);
                        count = 0;
                    }
                }

                @Override
                public void onFailure(Call<PlayListInfo> call, Throwable t) {

                }
            });
        }
    }



    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PlayListInfo playListInfo;
        playListInfo = list.get(i);
        Log.d(TAG, "playlist name: " + playListInfo.getItems().get(0).getSnippet().getChannelTitle());

        //send event
        EventBus.getDefault().post(new PlaylistItemClickEvent(playListInfo.getItems().get(0).getId()));
    }
}



