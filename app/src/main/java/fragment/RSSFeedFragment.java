package fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidteam.englishlearning.activity.EnglishApplication;
import com.androidteam.englishlearning.activity.FeedViewerActivity;
import com.voalearningenglish.net.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import adapter.FeedAdapter;
import models.RSSFeed;
import models.RssNew;
import network.ApiService;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import utils.Define;
import utils.HandleRSS;

//import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by HoangAnh on 10/06/2015.
 */
public class RSSFeedFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    final String TAG = getClass().getSimpleName();
    private static final String KEY_POSITION = "position";

    String url;
    String title;

    ListView lvFeed;
    List<RSSFeed> feeds;
    FeedAdapter adapter;
    ProgressBar progressBar;
    private EnglishApplication application;

    //    public Retrofit retrofit;
    public static OkHttpClient okHttpClient;

    public static Fragment getNewInstance(String url) {
        Fragment fragment = new RSSFeedFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Define.KEY_URL, url);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.feed_fragment, container, false);
        application = (EnglishApplication) view.getContext().getApplicationContext();
        init(view, savedInstanceState);

        return view;
    }

    private void init(View view, Bundle bundle) {
        //binding view
        lvFeed = (ListView) view.findViewById(R.id.lv_feed);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        feeds = new ArrayList<RSSFeed>();
        adapter = new FeedAdapter(getActivity().getApplicationContext(), feeds);
        lvFeed.setAdapter(adapter);
        lvFeed.setOnItemClickListener(this);

        bundle = getArguments();
        url = bundle.getString(Define.KEY_URL);
        Log.d("ANHTH", "url: " + url);

        new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {
                String response;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                try {
                    response = client.newCall(request).execute().body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                    response = null;
                }
//                Log.d("ANHTH", response.substring(0, 1000));
                Log.d("ANHTH", "leng: " + response.length());
                return response;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);
                progressBar.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(aVoid)) {
                    List<RSSFeed> rssFeedList = HandleRSS.getRssFeedsFromResponse(aVoid);
                    adapter.addData(rssFeedList);
                } else {
                    Toast.makeText(mContext, getString(R.string.error_connect), Toast.LENGTH_SHORT).show();
                }

            }
        }.execute(url);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(getActivity(), FeedViewerActivity.class);
        i.putExtra(Define.KEY_URL, feeds.get(position).link);
        i.putExtra(Define.KEY_TITLE, feeds.get(position).title);
        startActivity(i);
    }


}
