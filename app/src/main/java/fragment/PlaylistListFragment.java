package fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.androidteam.englishlearning.activity.EnglishApplication;
import com.voalearningenglish.net.R;

import java.util.ArrayList;
import java.util.List;

import adapter.PlayListAdapter;
import configuration.APIConfig;
import de.greenrobot.event.EventBus;
import event.PlaylistItemClickEvent;
import models.Chanel;
import models.EChanelType;
import result.playlistinfo.PlayListInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Define;
import utils.GetChannelList;
import utils.MakeToastMessage;

/**
 * Created by Admin on 15/4/2015.
 */
public class PlaylistListFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    static final String TAG = PlaylistListFragment.class.getSimpleName();

    private int count;
    private int total;

    private EChanelType chanelType;

    private List<Chanel> chanelList;
    private ListView lvChanelList;
    private PlayListAdapter adapter;
    private List<PlayListInfo> list;

    private ProgressBar progressBar;
    private EnglishApplication application;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_playlists, container, false);
        application = (EnglishApplication) view.getContext().getApplicationContext();
        count = 0;

        if (getArguments() == null) {
            MakeToastMessage.showMessageInShortTime(getActivity(), "Error.");
            chanelType = EChanelType.GRAMMAR;
        } else {
            Bundle bundle = getArguments();
            chanelType = (EChanelType) bundle.getSerializable(Define.KEY_SEND_CHANEL_TYPE);
        }

        init(view);
        loadListChannel();

        return view;
    }

    private void init(View v) {
        //bind view
        lvChanelList = (ListView) v.findViewById(R.id.lv_chanel);
        progressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        // set adapter
        list = new ArrayList<PlayListInfo>();
        chanelList = GetChannelList.getListChannel(chanelType);

        adapter = new PlayListAdapter(R.layout.item_playlist, list, getActivity(), chanelList);
        lvChanelList.setAdapter(adapter);
        lvChanelList.setOnItemClickListener(this);
    }

    private void loadListChannel() {
        //start loading
        progressBar.setVisibility(View.VISIBLE);
        for (Chanel c : chanelList) {
            count++;
            Call<PlayListInfo> call = application.apiService.getPlaylistDetail(APIConfig.API_KEY, c.id);
            call.enqueue(new Callback<PlayListInfo>() {
                @Override
                public void onResponse(Call<PlayListInfo> call, Response<PlayListInfo> response) {
                    if (response.body() != null) {
                        if (response.body().getItems() != null && !response.body().getItems().isEmpty()) {
                            list.add(response.body());
                            adapter.notifyDataSetChanged();
                        }
                        total++;
                    } else {
                        MakeToastMessage.showMessageInShortTime(getActivity(), "Error while loading");
                    }
                    if (total == count) {
                        progressBar.setVisibility(View.GONE);
                        count = 0;
                    }
                }

                @Override
                public void onFailure(Call<PlayListInfo> call, Throwable t) {

                }
            });
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PlayListInfo playListInfo;
        playListInfo = list.get(i);

        //send event
        EventBus.getDefault().post(new PlaylistItemClickEvent(playListInfo.getItems().get(0).getId()));
    }

    public static Fragment getNewInstance(EChanelType chanelType) {
        Fragment fragment = new PlaylistListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Define.KEY_SEND_CHANEL_TYPE, chanelType);
        fragment.setArguments(bundle);
        return fragment;
    }
}



