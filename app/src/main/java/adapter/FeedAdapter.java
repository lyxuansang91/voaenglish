package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import models.RSSFeed;
import models.RssNew;

/**
 * Created by HoangAnh on 09/06/2015.
 */
public class FeedAdapter extends BaseAdapter {

    Context context;
    List<RSSFeed> list;

    public FeedAdapter(Context context, List<RSSFeed> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_feed, parent, false);
            holder.imgFeed = (ImageView) convertView.findViewById(R.id.img_feed);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tv_description);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //display data
        RSSFeed rss = list.get(position);
        holder.tvDate.setText(rss.pubDate);
        holder.tvDescription.setText(rss.description);
        holder.tvTitle.setText(rss.title);
        Picasso.with(context).load(rss.urlImage).error(R.color.black).into(holder.imgFeed);
        return convertView;
    }

    public void addData(List<RSSFeed> feeds){
        list.clear();
        list.addAll(feeds);
        notifyDataSetChanged();
    }

    public void removeData(){
        list.clear();
        notifyDataSetChanged();
    }

    class ViewHolder {
        ImageView imgFeed;
        TextView tvDate, tvTitle, tvDescription;
    }
}


