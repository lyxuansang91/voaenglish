package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import result.playlistitem.Items;

/**
 * Created by Admin on 16/4/2015.
 */
public class VideoAdapter extends BaseAdapter {

    private static final String TAG = "Fragment";

    private int resource;
    private List<Items> list;
    private Context context;

    public VideoAdapter(int resource, List<Items> list, Context context) {
        this.resource = resource;
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Items item;
        View v = null;
        ItemHolder holder;

        item = list.get(i);
        if (view == null) {

            v = LayoutInflater.from(context).inflate(resource, viewGroup, false);
            holder = new ItemHolder(v);
            v.setTag(holder);
        } else {
            v = view;
            holder = (ItemHolder) v.getTag();
        }

        if(item != null) {
            holder.txtName.setText(item.getSnippet().getTitle());
            holder.txtDesc.setText(item.getSnippet().getDescription());
            String url = "";

            if(item.getSnippet().getThumbnails().getDefault() != null){
                url = item.getSnippet().getThumbnails().getDefault().getUrl();
            } else if(item.getSnippet().getThumbnails().getHigh() != null){
                url = item.getSnippet().getThumbnails().getHigh().getUrl();
            } else if(item.getSnippet().getThumbnails().getMedium() != null){
                url = item.getSnippet().getThumbnails().getMedium().getUrl();
            }
            if(url != null && !url.isEmpty())
            Picasso.with(context).load(url).into(holder.chanelImage);



        }
        return v;
    }

    class ItemHolder {

        public ItemHolder(View view) {
            chanelImage = (ImageView) view.findViewById(R.id.video_image);
            txtName = (TextView) view.findViewById(R.id.video_name);
            txtDesc = (TextView) view.findViewById(R.id.video_desc);
        }

        public ImageView chanelImage;
        public TextView txtName;
        public TextView txtDesc;
    }

    private boolean checkId(String id){

        return false;
    }
}