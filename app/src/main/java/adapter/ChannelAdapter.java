package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import event.ChannelItemClickEvent;
import models.CategoryDrawerListItem;

/**
 * Created by sang on 6/15/2015.
 */
public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ViewHolder> {

    private ArrayList<CategoryDrawerListItem> listChannel = new ArrayList<CategoryDrawerListItem>();
    private Context mContext;

    public ChannelAdapter(Context mContext, ArrayList<CategoryDrawerListItem> listChannel) {
        this.mContext = mContext;
        this.listChannel = listChannel;
    }

    @Override
    public ChannelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_drawerlist_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChannelAdapter.ViewHolder holder, int position) {
        CategoryDrawerListItem categoryDrawerListItem = listChannel.get(position);
        holder.tvCategoryName.setText(categoryDrawerListItem.getCategoryName());
        holder.ivCategoryName.setImageResource(categoryDrawerListItem.getCategoryImageUrl());
    }

    @Override
    public int getItemCount() {
        return listChannel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvCategoryName;
        ImageView ivCategoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCategoryName = (TextView) itemView.findViewById(R.id.tvCategoryName);
            ivCategoryName = (ImageView) itemView.findViewById(R.id.ivCategoryImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CategoryDrawerListItem categoryDrawerListItem = listChannel.get(getPosition());
                    EventBus.getDefault().post(new ChannelItemClickEvent(categoryDrawerListItem));
                }
            });
        }
    }
}
