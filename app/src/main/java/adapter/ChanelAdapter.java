package adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;

import java.util.HashMap;
import java.util.List;

import models.EChanelType;

public class ChanelAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> listDataHeader;
    private HashMap<String, List<EChanelType>> listDataChild;

    public ChanelAdapter(Context context, List<String> listDataHeader,
                         HashMap<String, List<EChanelType>> listChildData) {
        this._context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public EChanelType getChild(int groupPosition, int childPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final EChanelType childText = (EChanelType) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.elv_item);
        txtListChild.setTextColor(Color.BLACK);

        ImageView imgListChild = (ImageView) convertView.findViewById(R.id.item_chanel_img);

        if (childText != EChanelType.LEARN_ENGLISH_IN_A_FUN_WAY) {
            txtListChild.setText(childText.toString());
        } else {
            txtListChild.setText("FUN ENGLISH");
        }

        switch (childText){
            case PRONUNCIATION:
                imgListChild.setBackgroundResource(R.drawable.img_pronunciation);
                break;
            case VOCABULARY:
                imgListChild.setBackgroundResource(R.drawable.img_vocabulary);
                break;
            case SPEAKING:
                imgListChild.setBackgroundResource(R.drawable.img_speaking);
                break;
            case LISTENING:
                imgListChild.setBackgroundResource(R.drawable.img_listening);
                break;
            case READING:
                imgListChild.setBackgroundResource(R.drawable.img_reading);
                break;
            case WRITING:
                imgListChild.setBackgroundResource(R.drawable.img_writing);
                break;
            case GRAMMAR:
                imgListChild.setBackgroundResource(R.drawable.img_grammar);
                break;
            case IDIOMS:
                imgListChild.setBackgroundResource(R.drawable.img_idioms);
                break;
            case TOEIC:
                imgListChild.setBackgroundResource(R.drawable.img_toeic);
                break;
            case IELTS:
                imgListChild.setBackgroundResource(R.drawable.img_ielts);
                break;
            case TOEFL:
                imgListChild.setBackgroundResource(R.drawable.img_toefl);
                break;
            case LEARN_ENGLISH_IN_A_FUN_WAY:
                imgListChild.setBackgroundResource(R.drawable.img_fun_way);
                break;
            case OTHERS:
                imgListChild.setBackgroundResource(R.drawable.img_other);
                break;
        }



        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.item_group_chanel);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}