package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import models.RSSFeed;
import utils.RSSFeedURL;

/**
 * Created by HoangAnh on 16/06/2015.
 */
public class FeedCategoryAdapter extends BaseAdapter {

    Context context;
    List<RSSFeedURL> list;

    public FeedCategoryAdapter(Context context, List<RSSFeedURL> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_feed_category, parent, false);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_feed_category);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //display data
        RSSFeedURL rss = list.get(position);
        holder.tvName.setText(rss.name);

        return convertView;
    }

    public void addData(List<RSSFeedURL> feeds){
        list = feeds;
        notifyDataSetChanged();
    }

    public void removeData(){
        list.clear();
        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView tvName;
    }
}
