package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.voalearningenglish.net.R;
import com.androidteam.englishlearning.activity.EnglishApplication;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import configuration.Configuration;
import de.greenrobot.event.EventBus;
import event.DrawerListItemClickEvent;
import logcat.CLog;
import models.AccountDrawerListItem;
import models.CategoryDrawerListItem;
import models.DrawerListItem;
import models.HeaderDrawerListItem;
import models.LogoDrawerListItem;
import models.MenuDrawerListItem;


/**
 * Created by sang on 30/01/2015.
 */
public class DrawerListAdapter extends RecyclerView.Adapter<DrawerListAdapter.ViewHolder> {


    private static final String TAG = DrawerListAdapter.class.getSimpleName();

    ArrayList<DrawerListItem> mDrawerListItems = new ArrayList<DrawerListItem>();
    private Context mContext;


    public DrawerListAdapter(Context context, ArrayList<DrawerListItem> mDrawerListItems) {
        this.mDrawerListItems = mDrawerListItems;
        this.mContext = context;
    }


    @Override
    public int getItemViewType(int position) {
        CLog.d(TAG, "position:" + position);
        return this.mDrawerListItems.get(position).getType();
    }

    public void setData(int position, DrawerListItem item) {
        this.mDrawerListItems.set(position, item);
        this.notifyItemChanged(position);
    }

    public void add(int position, DrawerListItem item) {
        this.mDrawerListItems.add(position, item);
        this.notifyItemInserted(position);
    }

    public void add(DrawerListItem item) {
        this.mDrawerListItems.add(item);
        this.notifyItemInserted(mDrawerListItems.size() - 1);
    }

    public void remove(int position) {
        this.mDrawerListItems.remove(position);
        this.notifyItemRemoved(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int viewId = -1;
        switch (viewType) {
            case DrawerListItem.DRAWER_ACCOUNT_TYPE:
                viewId = R.layout.account_drawerlist_item;
                break;
            case DrawerListItem.DRAWER_CATEGORY_TYPE:
                viewId = R.layout.category_drawerlist_item;
                break;
            case DrawerListItem.DRAWER_MENU_TYPE:
                viewId = R.layout.menu_drawerlist_item;
                break;
            case DrawerListItem.DRAWER_HEADER_TYPE:
                viewId = R.layout.header_drawerlist_item;
                break;
            case DrawerListItem.DRAWER_LOGO_TYPE:
                viewId = R.layout.logo_drawerlist_item;
            default:
                break;
        }
        ViewHolder viewHolder = null;
        if (viewId != -1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
            viewHolder = new ViewHolder(view, viewType);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DrawerListItem drawerListItem = mDrawerListItems.get(position);
        switch (holder.getItemViewType()) {
            case DrawerListItem.DRAWER_CATEGORY_TYPE:
                holder.ivCategoryImage.setImageResource(((CategoryDrawerListItem) drawerListItem).getCategoryImageUrl());
                holder.tvCategoryName.setText(((CategoryDrawerListItem) drawerListItem).getCategoryName());

                if (EnglishApplication.getInstance().currentPosition != 0 && position == EnglishApplication.getInstance().currentPosition) {
                    holder.lnDrawerListItem.setBackgroundResource(R.drawable.xml_bg_drawer_item_active);
                } else
                    holder.lnDrawerListItem.setBackgroundResource(R.drawable.bg_drawer_item);
                break;
            case DrawerListItem.DRAWER_HEADER_TYPE:
                holder.tvHeaderName.setText(((HeaderDrawerListItem) drawerListItem).getHeaderName());
                break;
            case DrawerListItem.DRAWER_MENU_TYPE:
//                Picasso.with(mContext).load(((MenuDrawerListItem) drawerListItem).getMenuImageUrl()).error(R.drawable.ic_home).into(holder.ivMenuImage);
                if (((MenuDrawerListItem) drawerListItem).getMenuImageUrl() != 0) {
                    holder.ivMenuImage.setImageResource(((MenuDrawerListItem) drawerListItem).getMenuImageUrl());
                }
                holder.tvMenuName.setText(((MenuDrawerListItem) drawerListItem).getMenuName());
                if (EnglishApplication.getInstance().currentPosition != 0 && position == EnglishApplication.getInstance().currentPosition) {
                    holder.lnDrawerListItem.setBackgroundResource(R.drawable.xml_bg_drawer_item_active);
                } else
                    holder.lnDrawerListItem.setBackgroundResource(R.drawable.bg_drawer_item);
                break;
            case DrawerListItem.DRAWER_ACCOUNT_TYPE:
                if (((AccountDrawerListItem) drawerListItem).getAccountImageUrl() != null && !((AccountDrawerListItem) drawerListItem).getAccountImageUrl().isEmpty())
                    Picasso.with(mContext).load(((AccountDrawerListItem) drawerListItem).getAccountImageUrl()).into(holder.ivAccountImage);
                holder.tvAccountEmail.setText(((AccountDrawerListItem) drawerListItem).getAccountEmail());
                holder.tvAccountName.setText(((AccountDrawerListItem) drawerListItem).getAccountName());
                break;
            case DrawerListItem.DRAWER_LOGO_TYPE:
                holder.ivLogo.setImageResource(((LogoDrawerListItem) drawerListItem).getDrawableId());
                break;
            default:
                break;
        }

    }


    @Override
    public int getItemCount() {
        return mDrawerListItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircularImageView ivAccountImage;
        LinearLayout lnDrawerListItem;
        TextView tvAccountName;
        TextView tvAccountEmail;
        ImageView ivCategoryImage;
        TextView tvCategoryName;
        TextView tvCategoryDownload;
        TextView tvMenuName;
        ImageView ivMenuImage;
        TextView tvHeaderName;
        ImageView ivLogo;
        private View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerListItem mDrawerListItem = mDrawerListItems.get(getPosition());
                if ((mDrawerListItem != null) && (mDrawerListItem.getType() != DrawerListItem.DRAWER_HEADER_TYPE)) {
                    EventBus.getDefault().post(new DrawerListItemClickEvent(mDrawerListItem, getPosition()));
                }
            }
        };


        public ViewHolder(View v) {
            super(v);
        }

        public ViewHolder(View v, int type) {
            super(v);
            switch (type) {
                case DrawerListItem.DRAWER_ACCOUNT_TYPE:
                    ivAccountImage = (CircularImageView) v.findViewById(R.id.ivAccountImage);
                    tvAccountName = (TextView) v.findViewById(R.id.tvAccountName);
                    tvAccountEmail = (TextView) v.findViewById(R.id.tvAccountEmail);
                    break;
                case DrawerListItem.DRAWER_CATEGORY_TYPE:
                    tvCategoryName = (TextView) v.findViewById(R.id.tvCategoryName);
//                    tvCategoryDownload = (TextView) v.findViewById(R.id.tvCategoryDownload);
                    ivCategoryImage = (ImageView) v.findViewById(R.id.ivCategoryImage);
                    lnDrawerListItem = (LinearLayout) v.findViewById(R.id.lnDrawerListItem);
                    break;
                case DrawerListItem.DRAWER_HEADER_TYPE:
                    tvHeaderName = (TextView) v.findViewById(R.id.tvHeaderName);
                    break;
                case DrawerListItem.DRAWER_MENU_TYPE:
                    tvMenuName = (TextView) v.findViewById(R.id.tvMenuName);
                    ivMenuImage = (ImageView) v.findViewById(R.id.ivMenuImage);
                    lnDrawerListItem = (LinearLayout) v.findViewById(R.id.lnDrawerListItem);
                    break;
                case DrawerListItem.DRAWER_LOGO_TYPE:
                    ivLogo = (ImageView) v.findViewById(R.id.ivLogo);
                default:
                    break;
            }
            v.setOnClickListener(mOnClickListener);
        }
    }
}
