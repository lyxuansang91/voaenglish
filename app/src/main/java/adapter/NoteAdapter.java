package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import models.Note;
import result.playlistitem.Items;

/**
 * Created by Admin on 16/4/2015.
 */
public class NoteAdapter extends BaseAdapter {

    private static final String TAG = "Fragment";

    private int resource;
    private ArrayList<Note> list = new ArrayList<Note>();
    private Context context;

    public NoteAdapter(int resource, ArrayList<Note> list, Context context) {
        this.resource = resource;
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        View v = null;
        ItemHolder holder;

        Note item = list.get(i);
        if (view == null) {

            v = LayoutInflater.from(context).inflate(resource, viewGroup, false);
            holder = new ItemHolder(v);
            v.setTag(holder);
        } else {
            v = view;
            holder = (ItemHolder) v.getTag();
        }

        if(item != null) {
            holder.txtName.setText(item.getVideo_name());
            holder.txtDesc.setText(item.getNote_content());
            String url = item.getVideo_image();
            if(url != null && !url.isEmpty())
                Picasso.with(context).load(url).into(holder.chanelImage);
        }
        return v;
    }

    class ItemHolder {

        public ItemHolder(View view) {
            chanelImage = (ImageView) view.findViewById(R.id.video_image);
            txtName = (TextView) view.findViewById(R.id.video_name);
            txtDesc = (TextView) view.findViewById(R.id.video_desc);
        }

        public ImageView chanelImage;
        public TextView txtName;
        public TextView txtDesc;
    }

    private boolean checkId(String id){

        return false;
    }
}