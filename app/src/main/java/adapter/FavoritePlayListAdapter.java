package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.voalearningenglish.net.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import database.FavoritePlaylistDAO;
import models.Chanel;
import result.playlistinfo.PlayListInfo;

/**
 * Created by Admin on 15/4/2015.
 */
public class FavoritePlayListAdapter extends BaseAdapter {

    static final String TAG = PlayListAdapter.class.getSimpleName();

    private int resource;

    private List<String> favoriteList = new ArrayList<String>();
    private List<PlayListInfo> list;
    private Context context;
    private List<Chanel> chanelList;

    private FavoritePlaylistDAO dao;


    public FavoritePlayListAdapter(int resource, List<PlayListInfo> list, Context context, List<Chanel> chanelList) {
        this.resource = resource;
        this.list = list;
        this.context = context;
        this.chanelList = chanelList;
        dao = new FavoritePlaylistDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final PlayListInfo playListInfo;
        View v = null;
        final ItemHolder holder;

        playListInfo = list.get(i);

        if (view == null) {

            v = LayoutInflater.from(context).inflate(resource, viewGroup, false);
            holder = new ItemHolder(v);

            v.setTag(holder);
        } else {
            v = view;
            holder = (ItemHolder) v.getTag();
        }

        if (playListInfo.getItems() != null && !playListInfo.getItems().isEmpty()) {
            //display data
            holder.txtName.setText(chanelList.get(i).name);
            holder.txtViews.setText(playListInfo.getItems().get(0).getContentDetails().getItemCount() + " videos");
            Picasso.with(context).load(playListInfo.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl()).into(holder.chanelImage);

            if (checkIdIsInList(playListInfo.getItems().get(0).getId())) {
                holder.btnAddToFavorite.setVisibility(View.GONE);
            }

            holder.btnAddToFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    dao.open();
                                    dao.deleteFavoritePlaylist(playListInfo.getItems().get(0).getId());
                                    dao.close();

                                    list.remove(i);
                                    notifyDataSetChanged();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Do you want to remove this playlist?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();


                }
            });
        }
        return v;
    }


    private boolean checkIdIsInList(String id) {
        for (String tmp : favoriteList) {
            if (tmp.equals(id)) {
                return true;
            }
        }

        return false;
    }

    class ItemHolder {

        public ItemHolder(View view) {

            chanelImage = (ImageView) view.findViewById(R.id.chanel_image);
            txtName = (TextView) view.findViewById(R.id.chanel_name);
            txtViews = (TextView) view.findViewById(R.id.chanel_views);
            txtVideos = (TextView) view.findViewById(R.id.chanel_videos);
            btnAddToFavorite = (ImageButton) view.findViewById(R.id.chanel_addToFavorite);
        }

        public ImageView chanelImage;
        public TextView txtName;
        public TextView txtVideos;
        public TextView txtViews;
        public ImageButton btnAddToFavorite;
    }


}
