package network;

import models.RssNew;
import result.playlistinfo.PlayListInfo;
import result.playlistitem.PlayListItem;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by hoang on 12/16/2016.
 */
public interface ApiService {

//    @GET("/youtube/v3/playlists?key=%s&id=%s&part=id,contentDetails,snippet")
    @GET("/youtube/v3/playlists?part=id,contentDetails,snippet")
    Call<PlayListInfo> getPlaylistDetail(@Query("key") String key, @Query("id") String id);

    @GET("/youtube/v3/playlistItems?part=id,snippet,contentDetails")
    Call<PlayListItem> getPlaylistVideoList(@Query("key") String key, @Query("playlistId") String playlistId, @Query("pageToken") String pageToken);

    @GET
    Call<String> getRssFeed(@Url String url);
}
