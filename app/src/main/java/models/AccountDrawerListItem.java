package models;

/**
 * Created by sang on 30/01/2015.
 */
public class AccountDrawerListItem extends DrawerListItem {

    private String _accountImageUrl;
    private String _accountName;
    private String _accountEmail;
    private int _accountType;


    public AccountDrawerListItem(String _accountImageUrl, String _accountName, String _accountEmail, int type) {
        this._accountImageUrl = _accountImageUrl;
        this._accountName = _accountName;
        this._accountEmail = _accountEmail;
        this._accountType = type;
    }

    public AccountDrawerListItem() {
    }

    @Override
    public int getType() {
        return DRAWER_ACCOUNT_TYPE;
    }


    public String getAccountImageUrl() {
        return _accountImageUrl;
    }

    public void setAccountImageUrl(String _accountImageUrl) {
        this._accountImageUrl = _accountImageUrl;
    }

    public String getAccountName() {
        return _accountName;
    }

    public void setAccountName(String _accountName) {
        this._accountName = _accountName;
    }

    public String getAccountEmail() {
        return _accountEmail;
    }

    public void setAccountEmail(String _accountEmail) {
        this._accountEmail = _accountEmail;
    }

    public int getAccountType() {
        return _accountType;
    }

    public void setAccountType(int _accountType) {
        this._accountType = _accountType;
    }
}
