package models;

/**
 * Created by sang on 30/01/2015.
 */
public abstract class DrawerListItem {

    public static final int DRAWER_ACCOUNT_TYPE = 1;
    public static final int DRAWER_MENU_TYPE = 2;
    public static final int DRAWER_HEADER_TYPE = 3;
    public static final int DRAWER_CATEGORY_TYPE = 4;
    public static final int DRAWER_LOGO_TYPE = 5;

    public static final int ID_NOTE = 1;
    public static final int ID_FAVORITE_LIST = 2;
    public static final int ID_OTHER_APP = 3;
    public static final int ID_FAN_PAGE = 4;
    public static final int ID_RATING_FOR_US = 5;
    public static final int ID_SHARE_APP = 6;
    public static final int ID_ABOUT = 7;
    public static final int ID_HOME = 0;

    public abstract int getType();

}
