package models;

/**
 * Created by sang on 30/01/2015.
 */
public class CategoryDrawerListItem extends DrawerListItem {

    private int _categoryImageUrl;
    private String _categoryName;
    private String _categoryId;


    public CategoryDrawerListItem(int _categoryImageUrl, String _categoryName, String _categoryId) {
        this._categoryImageUrl = _categoryImageUrl;
        this._categoryName = _categoryName;
        this._categoryId = _categoryId;
    }

    public CategoryDrawerListItem() {
    }

    public int getCategoryImageUrl() {
        return _categoryImageUrl;
    }

    public void setCategoryImageUrl(int _categoryImageUrl) {
        this._categoryImageUrl = _categoryImageUrl;
    }

    public String getCategoryName() {
        return _categoryName;
    }

    public void setCategoryName(String _categoryName) {
        this._categoryName = _categoryName;
    }

    @Override
    public int getType() {
        return DRAWER_CATEGORY_TYPE;
    }

    public String getCategoryId() {
        return _categoryId;
    }

    public void setCategoryId(String _categoryId) {
        this._categoryId = _categoryId;
    }
}
