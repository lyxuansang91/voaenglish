package models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import java.util.List;

/**
 * Created by hoang on 1/9/2017.
 */

@Root(name = "rss", strict = false)
public class RssNew {


    @Element(name = "channel")
    public Channel channel;

    @Root(name = "channel")
    public static class Channel {
        @Element(name = "title")
        public String title;

        @Element(name = "link", required = false)
        public String link;

        @Element(name = "description", required = false)
        public String description;

        @Element(name = "image", required = false)
        public Image image;

        @Element(name = "language", required = false)
        public String language;

        @Element(name = "copyright", required = false)
        public String copyright;

        @Element(name = "ttl", required = false)
        public String ttl;

        @Element(name = "lastBuildDate", required = false)
        public String lastBuildDate;

        @Element(name = "generator", required = false)
        public String generator;

        @Element(name = "atom", required = false)
        @Namespace(prefix = "atom")
        public String atomlink;

        @ElementList(name = "item", required = false, inline = true)
        public List<Items> items;

    }

    public static class Items {
        @Element(name = "title", required = false)
        public String title;

        //        @Element(name = "link",required = false)
        @Path("link")
        @Text(required=false)
        public String link;

        @Element(name = "description", required = false)
        public String description;

        @Element(name = "enclosure", required = false)
        public Enclosure enclosure;

        @Element(name = "guid", required = false)
        public String guid;

        @Element(name = "comments", required = false)
        public String comments;

        @ElementList(name = "category", required = false)
        public List<String> category;

        @Element(name = "pubDate", required = false)
        public String pubDate;

        public Items() {
        }
    }

    public static class Enclosure {
        @Attribute(name = "url", required = false)
        public String urlImage;

        public Enclosure() {
        }
    }


    public static class Image {
        @Element(name = "url", required = false)
        public String url;

        @Element(name = "title", required = false)
        public String title;

//        @Element(name = "link", required = false)
        @Path("link")
        @Text(required=false)
        public String link = "";
    }


}



