package models;

/**
 * Created by sang on 09/04/2015.
 */
public class LogoDrawerListItem extends DrawerListItem{

    private int _drawableId;

    public LogoDrawerListItem(int drawableId) {
        this._drawableId = drawableId;
    }

    @Override
    public int getType() {
        return DRAWER_LOGO_TYPE;
    }

    public int getDrawableId() {
        return _drawableId;
    }

    public void setDrawableId(int _drawableId) {
        this._drawableId = _drawableId;
    }
}
