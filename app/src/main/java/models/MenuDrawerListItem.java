package models;

/**
 * Created by sang on 30/01/2015.
 */
public class MenuDrawerListItem extends DrawerListItem {

    private int _menuImageUrl;
    private String _menuName;
    private int _menuId;

    public MenuDrawerListItem(int _menuImageUrl, String _menuName) {
        this._menuImageUrl = _menuImageUrl;
        this._menuName = _menuName;
        this._menuId = 0;
    }

    public MenuDrawerListItem(int _menuImageUrl, String _menuName, int _menuId) {
        this._menuImageUrl = _menuImageUrl;
        this._menuName = _menuName;
        this._menuId = _menuId;
    }

    public MenuDrawerListItem() {
    }

    public int getMenuImageUrl() {
        return _menuImageUrl;
    }

    public void setMenuImageUrl(int _menuImageUrl) {
        this._menuImageUrl = _menuImageUrl;
    }

    public String getMenuName() {
        return _menuName;
    }

    public void setMenuName(String _menuName) {
        this._menuName = _menuName;
    }

    @Override
    public int getType() {
        return DRAWER_MENU_TYPE;
    }

    public int getMenuId() {
        return _menuId;
    }

    public void setMenuId(int _menuId) {
        this._menuId = _menuId;
    }
}
