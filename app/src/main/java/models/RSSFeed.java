package models;

/**
 * Created by HoangAnh on 09/06/2015.
 */
public class RSSFeed {
    public static final String TAG_ITEM = "item";
    public static final String TAG_TITLE = "title";
    public static final String TAG_DESCRIPTION = "description";
    public static final String TAG_LINK = "link";
    public static final String TAG_GUID = "guid";
    public static final String TAG_PUBDATE = "pubdate";
    public static final String TAG_CATEGORY = "category";
    public static final String TAG_COMMENTS = "comments";
    public static final String TAG_ENCLOSURE = "enclosure";

    public String item;
    public String title;
    public String description;
    public String link;
    public String guID;
    public String pubDate;
    public String category;
    public String comments;
    public String enclosure;
    public String urlImage;

    public RSSFeed() {

    }

}
