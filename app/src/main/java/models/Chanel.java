package models;

/**
 * Created by Admin on 15/4/2015.
 */
public class Chanel {
    public String id;
    public String url;
    public String name;
    public String description;
    public String imageUrl;

    public Chanel(String id, String url, String name, String description, String imageUrl) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
    }
}