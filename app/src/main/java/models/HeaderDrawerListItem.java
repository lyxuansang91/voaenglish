package models;

/**
 * Created by sang on 02/02/2015.
 */
public class HeaderDrawerListItem extends DrawerListItem {

    private String _HeaderName;

    public HeaderDrawerListItem(String _HeaderName) {
        this._HeaderName = _HeaderName;
    }

    public HeaderDrawerListItem() {
    }

    public String getHeaderName() {
        return _HeaderName;
    }

    public void setHeaderName(String _HeaderName) {
        this._HeaderName = _HeaderName;
    }

    @Override
    public int getType() {
        return DRAWER_HEADER_TYPE;
    }
}
