package models;

/**
 * Created by sang on 29/04/2015.
 */
public class Note {
    private String video_id;
    private String note_content;
    private String video_name;
    private Long created_time;
    private Long updated_time;
    private String video_image;

    public String getVideo_image() {
        return video_image;
    }

    public void setVideo_image(String video_image) {
        this.video_image = video_image;
    }


    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public Long getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Long created_time) {
        this.created_time = created_time;
    }

    public Long getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Long updated_time) {
        this.updated_time = updated_time;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getNote_content() {
        return note_content;
    }

    public void setNote_content(String note_content) {
        this.note_content = note_content;
    }

    public Note(String video_id, String note_content) {
        this.video_id = video_id;
        this.note_content = note_content;
    }

    public Note(String video_id, String note_content, String video_image) {
        this.video_id = video_id;
        this.note_content = note_content;
        this.video_image = video_image;
    }

    public Note(String video_id, String video_name, String note_content, String video_image) {
        this.video_id = video_id;
        this.video_name = video_name;
        this.note_content = note_content;
        this.video_image = video_image;
    }

    public Note(String video_id, String note_content, Long created_time, Long updated_time) {
        this.video_id = video_id;
        this.note_content = note_content;
        this.created_time = created_time;
        this.updated_time = updated_time;
    }

    public Note() {
    }
}
