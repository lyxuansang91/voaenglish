package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 19/4/2015.
 */
public class FavoritePlaylistDAO {

    private SQLiteDatabase sqLiteDatabase;
    private FavoritePlaylistOpenHelper favoritePlaylistOpenHelper;

    public FavoritePlaylistDAO(Context context) {
        favoritePlaylistOpenHelper = new FavoritePlaylistOpenHelper(context);
    }

    public void open() throws SQLException {
        sqLiteDatabase = favoritePlaylistOpenHelper.getWritableDatabase();
    }

    public void close() {
        favoritePlaylistOpenHelper.close();
    }

    public boolean addFavoritePlaylist(String id) {
        ContentValues values = new ContentValues();
        values.put(favoritePlaylistOpenHelper.COLUMN_ID, id);

        long status = sqLiteDatabase.insert(favoritePlaylistOpenHelper.TABLE_NAME, null,
                values);
        if (status == -1) return false;
        return true;
    }

    public boolean deleteFavoritePlaylist(String id) {
        int status = sqLiteDatabase.delete(favoritePlaylistOpenHelper.TABLE_NAME, favoritePlaylistOpenHelper.COLUMN_ID + " = " + id, null);
        if (status == -1) return false;
        return true;
    }

    public List<String> findAll() {
        List<String> list;
        Cursor cursor;

        list = new ArrayList<>();

        cursor = sqLiteDatabase.query(favoritePlaylistOpenHelper.TABLE_NAME, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                list.add(cursor.getString(0));
                cursor.moveToNext();
            }
        }
        return list;
    }

}
