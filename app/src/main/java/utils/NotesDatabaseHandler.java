package utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import models.Note;

/**
 * Created by sang on 15/12/2014.
 */
public class NotesDatabaseHandler extends SQLiteOpenHelper {

    //Database version
    private static final int DATABASE_VERSION = 2;

    //Database name
    private static final String DATABASE_NAME = "Note";

    //Songs table name
    private static final String TABLE_NOTE = "Note";

    //Song table columns names
    private static final String KEY_VIDEO_ID = "video_id";
    private static final String KEY_VIDEO_NAME = "video_name";
    private static final String KEY_NOTE_CONTENT = "note_content";
    private static final String KEY_CREATED_TIME = "created_time";
    private static final String KEY_UPDATED_TIME = "updated_time";
    private static final String KEY_VIDEO_IMAGE = "video_image";


    public NotesDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SONGS_TABLE =
                "CREATE TABLE " + TABLE_NOTE + "("
                + KEY_VIDEO_ID + " TEXT PRIMARY KEY, "
                + KEY_VIDEO_NAME + " TEXT, "
                + KEY_NOTE_CONTENT + " TEXT, "
                + KEY_VIDEO_IMAGE + " TEXT, "
                + KEY_CREATED_TIME + " INTEGER, "
                + KEY_UPDATED_TIME + " INTEGER)";
        sqLiteDatabase.execSQL(CREATE_SONGS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTE);
        onCreate(db);
    }


    public boolean addNote(Note note) {
        if(note == null)
            return false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VIDEO_ID, note.getVideo_id());
        values.put(KEY_NOTE_CONTENT, note.getNote_content());
        values.put(KEY_VIDEO_NAME, note.getVideo_name());
        values.put(KEY_VIDEO_IMAGE, note.getVideo_image());
        values.put(KEY_CREATED_TIME, System.currentTimeMillis());
        values.put(KEY_UPDATED_TIME, System.currentTimeMillis());
        long row_id = db.insert(TABLE_NOTE, null, values);
        db.close();
        return (row_id != -1);
    }




    public Note getNoteByID(String video_id){
        Note note = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NOTE, new String[]{KEY_VIDEO_ID, KEY_NOTE_CONTENT}, KEY_VIDEO_ID + " = ?",
                new String[]{video_id}, null, null, null, null);

        boolean isExisted = false;
        if(cursor != null && cursor.moveToFirst()) {
            note = new Note();
            note.setVideo_id(cursor.getString(cursor.getColumnIndex(KEY_VIDEO_ID)));
            note.setNote_content(cursor.getString(cursor.getColumnIndex(KEY_NOTE_CONTENT)));
//            note.setCreated_time(cursor.getLong(cursor.getColumnIndex(KEY_CREATED_TIME)));
//            note.setUpdated_time(cursor.getLong(cursor.getColumnIndex(KEY_UPDATED_TIME)));
        }
        cursor.close();
        db.close();
        return note;
    }

    public boolean updateNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(KEY_NOTE_CONTENT, note.getNote_content());
        content.put(KEY_UPDATED_TIME, System.currentTimeMillis());
        int num_row_updated = db.update(TABLE_NOTE, content, KEY_VIDEO_ID +  " = ? ", new String[]{note.getVideo_id()});
        db.close();
        return (num_row_updated > 0);
    }


    public ArrayList<Note> getAllNotes() {
        ArrayList<Note> notesList = new ArrayList<Note>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NOTE, new String[]{KEY_VIDEO_ID, KEY_VIDEO_NAME, KEY_NOTE_CONTENT, KEY_VIDEO_IMAGE, KEY_CREATED_TIME, KEY_UPDATED_TIME}, null, null, null, null, null);
        if(cursor != null && cursor.moveToFirst()) {
            notesList = new ArrayList<Note>();
            do {
                Note note = new Note();
                note.setVideo_id(cursor.getString(cursor.getColumnIndex(KEY_VIDEO_ID)));
                note.setVideo_name(cursor.getString(cursor.getColumnIndex(KEY_VIDEO_NAME)));
                note.setNote_content(cursor.getString(cursor.getColumnIndex(KEY_NOTE_CONTENT)));
                note.setVideo_image(cursor.getString(cursor.getColumnIndex(KEY_VIDEO_IMAGE)));
//                note.setCreated_time(cursor.getLong(cursor.getColumnIndex(KEY_CREATED_TIME)));
//                note.setUpdated_time(cursor.getLong(cursor.getColumnIndex(KEY_UPDATED_TIME)));
                notesList.add(note);
            } while(cursor.moveToNext());
            cursor.close();
        }
//        cursor.close();
        db.close();
        return notesList;
    }

    public boolean removeNote(String video_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int num_row_delete = db.delete(TABLE_NOTE, KEY_VIDEO_ID + " = ?", new String[]{video_id});
        db.close();
        return (num_row_delete > 0);
    }


}
