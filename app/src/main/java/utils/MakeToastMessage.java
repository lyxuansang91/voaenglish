package utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Admin on 17/4/2015.
 */
public class MakeToastMessage {

    public static void showMessageInShortTime(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
    public static void showMessageInLongTime(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }

}
