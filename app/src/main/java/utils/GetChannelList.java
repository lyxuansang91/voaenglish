package utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import models.Chanel;
import models.EChanelType;

/**
 * Created by Admin on 15/4/2015.
 */
public class GetChannelList {

    public static List<Chanel> getListChannel(EChanelType type) {
        switch (type) {
            case PRONUNCIATION:
//                    return getAll();
                return getPronunciation();
            case VOCABULARY:
                return getVocabulary();
            case SPEAKING:
                return getSpeaking();
            case LISTENING:
                return getListening();
            case READING:
                return getReading();
            case WRITING:
                return getWriting();
            case GRAMMAR:
                return getGrammar();
            case IDIOMS:
                return getIdioms();
            case TOEIC:
                return getTOEIC();
            case IELTS:
                return getIELTS();
            case TOEFL:
                return getTOEFL();
            case LEARN_ENGLISH_IN_A_FUN_WAY:
                return getFunWay();
            case OTHERS:
                return getAllCategory();
//                return getOthers();
            default:
                List<Chanel> list = new ArrayList<Chanel>();
                list.addAll(getPronunciation());
                list.addAll(getVocabulary());
                list.addAll(getSpeaking());
                list.addAll(getListening());
                list.addAll(getReading());
                list.addAll(getWriting());
                list.addAll(getGrammar());
                list.addAll(getIdioms());
                list.addAll(getTOEIC());
                list.addAll(getIELTS());
                list.addAll(getTOEFL());
                list.addAll(getFunWay());
                list.addAll(getOthers());

                return list;
        }
    }

    public static List<Chanel> findByKey(String key) {
        List<Chanel> list;

        list = new ArrayList<Chanel>();

        if (key == null || key.trim().toLowerCase().equals("")) {
            list = getListChannel(EChanelType.ALL);
        } else {
            List<Chanel> tmps = getListChannel(EChanelType.ALL);
            for (int i = 0; i < tmps.size(); i++) {
                if (tmps.get(i).name.trim().toLowerCase().contains(key.trim().toLowerCase())) {
                    list.add(tmps.get(i));
                }
            }
        }

        Log.d("SEARCH LIST:",""+list.size());

        return list;
    }

    public static List<Chanel> getPronunciation() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLD6B222E02447DC07", "https://www.youtube.com/playlist?list=PLD6B222E02447DC07", "Learn the sounds of English", "", ""));
        list.add(new Chanel("PLRdDJFkjVSxO9iVDuikz3ljBuCOsVl0MQ", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxO9iVDuikz3ljBuCOsVl0MQ", "Master Spoken English", "", ""));
        list.add(new Chanel("PL5B1B05ECFE94BEF5", "https://www.youtube.com/playlist?list=PL5B1B05ECFE94BEF5", "The Most Common Words in English", "", ""));
        list.add(new Chanel("PL0B58DCD9199D5668", "https://www.youtube.com/playlist?list=PL0B58DCD9199D5668", "Jennifer-English pronunciation", "", ""));
        list.add(new Chanel("PLkvBxZ4952-8EG-2J7-UCEpY1EoxXXoGv", "https://www.youtube.com/playlist?list=PLkvBxZ4952-8EG-2J7-UCEpY1EoxXXoGv", "Global Accents Tips", "", ""));
        list.add(new Chanel("PLpLRk365gbPYp6asP_n8ru-D6WWje4mCB", "https://www.youtube.com/playlist?list=PLpLRk365gbPYp6asP_n8ru-D6WWje4mCB", "How to sound like a native English speaker", "", ""));
        return list;
    }

    public static List<Chanel> getVocabulary() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLRdDJFkjVSxMnDUH74nny75WT-wGzSsxf", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxMnDUH74nny75WT-wGzSsxf", "English Vocabulary from A to Z", "", ""));
        list.add(new Chanel("ELqqsD54NW2bM", "https://www.youtube.com/playlist?list=ELqqsD54NW2bM", "Daily Video Vocabulary Season 1", "", ""));
        list.add(new Chanel("ELEsqv4iynoG4", "https://www.youtube.com/playlist?list=ELEsqv4iynoG4", "Daily Video Vocabulary Season 2", "", ""));
        list.add(new Chanel("ELEBsOtN4DqUg", "https://www.youtube.com/playlist?list=ELEBsOtN4DqUg", "Daily Video Vocabulary Season 3", "", ""));
        list.add(new Chanel("ELKdNaCmnZkhE", "https://www.youtube.com/playlist?list=ELKdNaCmnZkhE", "Daily Video Vocabulary Season 4", "", ""));
        list.add(new Chanel("PLpLRk365gbPZNsx8twWQ-bAuKOJ3wJ760", "https://www.youtube.com/playlist?list=PLpLRk365gbPZNsx8twWQ-bAuKOJ3wJ760", "Real English Vocabulary", "", ""));
        list.add(new Chanel("PL1MxVBsQo85q6Yb2v9hLIurN6nm7vTBMi", "ttps://www.youtube.com/playlist?list=PL1MxVBsQo85q6Yb2v9hLIurN6nm7vTBMi", "English Vocabulary with James", "", ""));

        return list;
    }

    public static List<Chanel> getSpeaking() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLpLRk365gbPZnk7MF4xX0ZwoTUoOazsT6", "https://www.youtube.com/playlist?list=PLpLRk365gbPZnk7MF4xX0ZwoTUoOazsT6", "Speaking REAL English", "", ""));
        list.add(new Chanel("PLF467B6C12B713A03", "https://www.youtube.com/playlist?list=PLF467B6C12B713A03", "Speak English With Mister Duncan", "", ""));
        list.add(new Chanel("PLRdDJFkjVSxMJSuBTZwTWJAIg7z_4T3QK", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxMJSuBTZwTWJAIg7z_4T3QK", "English Today", "", ""));
        list.add(new Chanel("PL1MxVBsQo85r0c30_dRS54444vIKWrCju", "https://www.youtube.com/playlist?list=PL1MxVBsQo85r0c30_dRS54444vIKWrCju", "Speaking English with James", "", ""));

        return list;
    }

    public static List<Chanel> getListening() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLRdDJFkjVSxMJSuBTZwTWJAIg7z_4T3QK", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxMJSuBTZwTWJAIg7z_4T3QK", "English Today", "", ""));
        list.add(new Chanel("PL6274A3AF6C5A4234", "https://www.youtube.com/playlist?list=PL6274A3AF6C5A4234", "Listening English (Teacher Phil)", "", ""));
        list.add(new Chanel("PLefT-APcu6wfD5Nbw9BpFSyN7hBNcZhR3", "https://www.youtube.com/playlist?list=PLefT-APcu6wfD5Nbw9BpFSyN7hBNcZhR3", "Listening Level 1", "", ""));
        list.add(new Chanel("PLefT-APcu6wcngETanZ56MOYN43fbdTyu", "https://www.youtube.com/playlist?list=PLefT-APcu6wcngETanZ56MOYN43fbdTyu", "Listening Level 2", "", ""));
        list.add(new Chanel("PLefT-APcu6wdJlHKbrvLuzrWTdd3LUB_f", "https://www.youtube.com/playlist?list=PLefT-APcu6wdJlHKbrvLuzrWTdd3LUB_f", "Listening Level 3", "", ""));
        list.add(new Chanel("PL6BDo90oiwpRdmnAzmYwdc0Az0ZOG2XNA", "https://www.youtube.com/playlist?list=PL6BDo90oiwpRdmnAzmYwdc0Az0ZOG2XNA", "English listening practice", "", ""));


        return list;
    }

    public static List<Chanel> getReading() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLRdDJFkjVSxMJSuBTZwTWJAIg7z_4T3QK", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxMJSuBTZwTWJAIg7z_4T3QK", "English Today", "", ""));
        list.add(new Chanel("PLfQSN9FlyB6RumUTLuDAGY3m6YpBLHSsw", "https://www.youtube.com/playlist?list=PLfQSN9FlyB6RumUTLuDAGY3m6YpBLHSsw", "Reading Fluency in English", "", ""));
        list.add(new Chanel("PLE6gQE-Y0vNCaFbnDL47j-p-xqGtL79Ne", "https://www.youtube.com/playlist?list=PLE6gQE-Y0vNCaFbnDL47j-p-xqGtL79Ne", "Reading & Spelling", "", ""));
        list.add(new Chanel("PL1ECF842014D455FD", "https://www.youtube.com/playlist?list=PL1ECF842014D455FD", "English Stories to read along", "", ""));

        return list;
    }

    public static List<Chanel> getWriting() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLfQSN9FlyB6Q8MTULKyIHzqKeXpBbxES6", "https://www.youtube.com/playlist?list=PLfQSN9FlyB6Q8MTULKyIHzqKeXpBbxES6", "English Writing Skills", "", ""));
        list.add(new Chanel("PLxYD9HaZwsI5C0d8CivHvoI_-0rs8XMfc", "https://www.youtube.com/playlist?list=PLxYD9HaZwsI5C0d8CivHvoI_-0rs8XMfc", "Learn English with Adam", "", ""));
        list.add(new Chanel("PLN3kZ8bfmMJN2-EdLyE7_rOZo8o3IpFlv", "https://www.youtube.com/playlist?list=PLN3kZ8bfmMJN2-EdLyE7_rOZo8o3IpFlv", "Academic Writing", "", ""));
        list.add(new Chanel("PL-PLYN9SJQeLzqSFgWiBCulyyz9BeQxF7", "https://www.youtube.com/playlist?list=PL-PLYN9SJQeLzqSFgWiBCulyyz9BeQxF7", "English Grammar and Writing", "", ""));

        return list;
    }

    public static List<Chanel> getGrammar() {

        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PL1MxVBsQo85rDkYYIaWxScsAFSk6vj90N", "https://www.youtube.com/playlist?list=PL1MxVBsQo85rDkYYIaWxScsAFSk6vj90N", "English Grammar with James", "", ""));
        list.add(new Chanel("PLpLRk365gbPaY0U_9zYYRY5JmStMZ9NSI", "https://www.youtube.com/playlist?list=PLpLRk365gbPaY0U_9zYYRY5JmStMZ9NSI", "English Grammar with Ronnie", "", ""));
        list.add(new Chanel("PLxSz4mPLHWDYoqVgOxNqjozVcwW5YLTAH", "https://www.youtube.com/playlist?list=PLxSz4mPLHWDYoqVgOxNqjozVcwW5YLTAH", "English Grammar with Rebecca", "", ""));
        list.add(new Chanel("PLEEF1506D9AD34B01", "https://www.youtube.com/playlist?list=PLEEF1506D9AD34B01", "English Grammar Lessons", "", ""));

        return list;
    }

    public static List<Chanel> getIdioms() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLE47AEA94D122609F", "https://www.youtube.com/watch?v=cskIoQg8LCw&list=PLE47AEA94D122609F", "Animal Idioms", "", ""));
        list.add(new Chanel("PLC27FE4C42575B2DE", "https://www.youtube.com/watch?v=p0Ygbm2GuHk&list=PLC27FE4C42575B2DE", "Body Idioms 1", "", ""));
        list.add(new Chanel("PL334A9657B9AE88E3", "https://www.youtube.com/watch?v=_9Qtrihd8UU&list=PL334A9657B9AE88E3", "Body Idioms 2", "", ""));
        list.add(new Chanel("PL5945726A989B480B", "https://www.youtube.com/watch?v=xWVbSX8QVNI&list=PL5945726A989B480B", "Food Idioms", "", ""));
        list.add(new Chanel("PLC53C7B5067C7A5CB", "https://www.youtube.com/watch?v=E4WdazLU3Kw&list=PLC53C7B5067C7A5CB", "Sport Idioms", "", ""));
        list.add(new Chanel("PL9C992C4B08DE45A5", "https://www.youtube.com/watch?v=1LOiHFqUDPQ&list=PL9C992C4B08DE45A5", "Transport Idioms", "", ""));
        list.add(new Chanel("PLpLRk365gbPaslG6vcDsKjwyoPMDctzw7", "https://www.youtube.com/playlist?list=PLpLRk365gbPaslG6vcDsKjwyoPMDctzw7", "Slang and BAD words in English", "", ""));

        return list;
    }

    public static List<Chanel> getTOEIC() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PL-5dIdLOa2jbrM-13QqN2sds0H4vO46Ne", "https://www.youtube.com/playlist?list=PL-5dIdLOa2jbrM-13QqN2sds0H4vO46Ne", "TOEIC Reading Skills", "", ""));
        list.add(new Chanel("PLF620FBC8F3E53C34", "https://www.youtube.com/playlist?list=PLF620FBC8F3E53C34", "Lesson IETLS TOEIC", "", ""));
        list.add(new Chanel("PLxSz4mPLHWDaU-hMVZ7p3SiQvnEDI1jpQ", "https://www.youtube.com/playlist?list=PLxSz4mPLHWDaU-hMVZ7p3SiQvnEDI1jpQ", "Get a high score on IELTS, TOEFL,TOEIC", "", ""));
        list.add(new Chanel("PLSFgK8I59IABEV5yU_qDxL9ikv56OYsOM", "https://www.youtube.com/playlist?list=PLSFgK8I59IABEV5yU_qDxL9ikv56OYsOM", "TOEIC Listening Practice", "", ""));
        list.add(new Chanel("PLLLBCC91v9d035IqR4EQOZfz-aswHe7Iy", "https://www.youtube.com/playlist?list=PLLLBCC91v9d035IqR4EQOZfz-aswHe7Iy", "TOEIC Listening Test", "", ""));
        list.add(new Chanel("PLBCBA2850FBFB92B5", "https://www.youtube.com/playlist?list=PLBCBA2850FBFB92B5", "TOEIC Test", "", ""));

        return list;
    }

    public static List<Chanel> getIELTS() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PL01269B14A762A15F", "https://www.youtube.com/playlist?list=PL01269B14A762A15F", "ELTS Preparation Series 1", "", ""));
        list.add(new Chanel("PLED8BF37E68E31EF3", "https://www.youtube.com/playlist?list=PLED8BF37E68E31EF3", "ELTS Preparation Series 2", "", ""));
        list.add(new Chanel("PL2VKY1hoGa6mhBSK55iVHnv1aDgDVI3nV", "https://www.youtube.com/playlist?list=PL2VKY1hoGa6mhBSK55iVHnv1aDgDVI3nV", "REAL IELTS SPEAKING TEST", "", ""));
        list.add(new Chanel("PLF620FBC8F3E53C34", "https://www.youtube.com/playlist?list=PLF620FBC8F3E53C34", "Lesson IETLS TOEIC", "", ""));
        list.add(new Chanel("PLxSz4mPLHWDaU-hMVZ7p3SiQvnEDI1jpQ", "https://www.youtube.com/playlist?list=PLxSz4mPLHWDaU-hMVZ7p3SiQvnEDI1jpQ", "Get a high score on IELTS, TOEFL,TOEIC", "", ""));

        return list;
    }

    public static List<Chanel> getTOEFL() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PL499345C34BF71B4C", "https://www.youtube.com/playlist?list=PL499345C34BF71B4C", "Inside the TOEFL Test", "", ""));
        list.add(new Chanel("PLneKE8RzKfcmNmfiq5s6GsRSi9vyPx9jS", "https://www.youtube.com/playlist?list=PLneKE8RzKfcmNmfiq5s6GsRSi9vyPx9jS", "Prepare for the TOEFL test", "", ""));
        list.add(new Chanel("PL4pd-gpq-YUGH5T5Gz7kUeY3tcXbCEHdx", "https://www.youtube.com/playlist?list=PL4pd-gpq-YUGH5T5Gz7kUeY3tcXbCEHdx", "TOEFL listening practice", "", ""));
        list.add(new Chanel("PL7cyaJTKhUnw0UpwbwtSr9mPIIbk8sLgi", "https://www.youtube.com/playlist?list=PL7cyaJTKhUnw0UpwbwtSr9mPIIbk8sLgi", "TOEFL Reading Skills Preparation", "", ""));
        list.add(new Chanel("PL7cyaJTKhUnzDLkiaiH5X0ymr7zE620xb", "https://www.youtube.com/playlist?list=PL7cyaJTKhUnzDLkiaiH5X0ymr7zE620xb", "TOEFL Speaking Skills Preparation", "", ""));
        list.add(new Chanel("PLxSz4mPLHWDaU-hMVZ7p3SiQvnEDI1jpQ", "https://www.youtube.com/playlist?list=PLxSz4mPLHWDaU-hMVZ7p3SiQvnEDI1jpQ", "Get a high score on IELTS, TOEFL,TOEIC", "", ""));

        return list;
    }

    public static List<Chanel> getFunWay() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLRdDJFkjVSxMTET3w1U6PmqwW6WsxrBMJ", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxMTET3w1U6PmqwW6WsxrBMJ", "Extra English Movie", "", ""));
        list.add(new Chanel("PL8E7uzvkLxaXg9RMJ29_L1zlvSB0rACTj", "https://www.youtube.com/playlist?list=PL8E7uzvkLxaXg9RMJ29_L1zlvSB0rACTj", "Comedy English videos", "", ""));
        list.add(new Chanel("PL_T5Xt9SBbEhSYSZCldJ5j3IFYhrvNM9_", "https://www.youtube.com/playlist?list=PL_T5Xt9SBbEhSYSZCldJ5j3IFYhrvNM9_", "Popular British comedy", "", ""));
        list.add(new Chanel("PL160964AA4607CAAE", "https://www.youtube.com/playlist?list=PL160964AA4607CAAE", "Real English", "", ""));

        return list;
    }

    public static List<Chanel> getOthers() {
        List<Chanel> list = new ArrayList<Chanel>();
        list.add(new Chanel("PLd9hCvj34W5hJZwjdghwQQi3XhpSZO9sx", "https://www.youtube.com/playlist?list=PLd9hCvj34W5hJZwjdghwQQi3XhpSZO9sx", "English in a Minute", "", ""));
        list.add(new Chanel("PLd9hCvj34W5gR8u7liXqZeWLsVnhR5ZPV", "https://www.youtube.com/playlist?list=PLd9hCvj34W5gR8u7liXqZeWLsVnhR5ZPV", "American Stories", "", ""));
        list.add(new Chanel("PLd9hCvj34W5ivxvT11b0SCnS_tyhgjbbr", "https://www.youtube.com/playlist?list=PLd9hCvj34W5ivxvT11b0SCnS_tyhgjbbr", "Voices of America", "", ""));
        list.add(new Chanel("PL2CF36C5049764FE0", "https://www.youtube.com/playlist?list=PL2CF36C5049764FE0", "Voa special English", "", ""));
        list.add(new Chanel("PLRdDJFkjVSxNmafrNd4mXxLxapG4lMKtJ", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxNmafrNd4mXxLxapG4lMKtJ", "History of English", "", ""));

        return list;
    }

    public static List<Chanel> getAllCategory() {
        List<Chanel> list = new ArrayList<Chanel>();
        
//        list.add(new Chanel("PLD6B222E02447DC07", "https://www.youtube.com/playlist?list=PLD6B222E02447DC07", "Learn the sounds of English", "", ""));
//        list.add(new Chanel("PLRdDJFkjVSxO9iVDuikz3ljBuCOsVl0MQ", "https://www.youtube.com/playlist?list=PLRdDJFkjVSxO9iVDuikz3ljBuCOsVl0MQ", "Master Spoken English", "", ""));
//        list.add(new Chanel("PL5B1B05ECFE94BEF5", "https://www.youtube.com/playlist?list=PL5B1B05ECFE94BEF5", "The Most Common Words in English", "", ""));
//        list.add(new Chanel("PL0B58DCD9199D5668", "https://www.youtube.com/playlist?list=PL0B58DCD9199D5668", "Jennifer-English pronunciation", "", ""));
//        list.add(new Chanel("PLkvBxZ4952-8EG-2J7-UCEpY1EoxXXoGv", "https://www.youtube.com/playlist?list=PLkvBxZ4952-8EG-2J7-UCEpY1EoxXXoGv", "Global Accents Tips", "", ""));
//        list.add(new Chanel("PLpLRk365gbPYp6asP_n8ru-D6WWje4mCB", "https://www.youtube.com/playlist?list=PLpLRk365gbPYp6asP_n8ru-D6WWje4mCB", "How to sound like a native English speaker", "", ""));
        return list;
    }
}
