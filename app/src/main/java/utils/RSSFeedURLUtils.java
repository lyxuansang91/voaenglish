package utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HoangAnh on 16/06/2015.
 */
public class RSSFeedURLUtils {

    public static List<RSSFeedURL> getAllFeed(){
        List<RSSFeedURL> list;

        list = new ArrayList<RSSFeedURL>();
        list.add(new RSSFeedURL("All zones","http://learningenglish.voanews.com/api/epiqq"));
        list.add(new RSSFeedURL("World","http://learningenglish.voanews.com/api/zymppeq$pp"));
        list.add(new RSSFeedURL("USA","http://learningenglish.voanews.com/api/zvmpmeo$pm"));
        list.add(new RSSFeedURL("Business","http://learningenglish.voanews.com/api/z_mpier$pi"));
        list.add(new RSSFeedURL("Education","http://learningenglish.voanews.com/api/ztmp_ei$p_"));
        list.add(new RSSFeedURL("Everyday Grammar","http://learningenglish.voanews.com/api/zoroqqegtoqq"));
        list.add(new RSSFeedURL("Health & Lifestyle","http://learningenglish.voanews.com/api/zmmpqe$-po"));
        list.add(new RSSFeedURL("Entertainment","http://learningenglish.voanews.com/api/z$mpoet$pi"));
        list.add(new RSSFeedURL("Science & Technology","http://learningenglish.voanews.com/api/zpmpre--pq"));
        list.add(new RSSFeedURL("As It Is","http://learningenglish.voanews.com/api/zkm-qem$-o"));
        list.add(new RSSFeedURL("American Mosaic","http://learningenglish.voanews.com/api/zpyp_e-rm_"));
        list.add(new RSSFeedURL("In the News","http://learningenglish.voanews.com/api/zig_oejmyo"));
        list.add(new RSSFeedURL("The Making of a Nation","http://learningenglish.voanews.com/api/zj_pveyrmv"));
        list.add(new RSSFeedURL("Science in the News","http://learningenglish.voanews.com/api/zmg_pe$myp"));
        list.add(new RSSFeedURL("This Is America","http://learningenglish.voanews.com/api/z_g_mer_ym"));
        list.add(new RSSFeedURL("Words and Their Stories","http://learningenglish.voanews.com/api/zmypye$rmy"));
        list.add(new RSSFeedURL("American Stories ","http://learningenglish.voanews.com/api/zyg__eq_y_"));

        return list;
    }
}
