package utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HoangAnh on 09/06/2015.
 */
public class RSSFeedURL {
    public String name;
    public String url;

    public RSSFeedURL(String name,String url){
        this.name = name;
        this.url = url;
    }

}
