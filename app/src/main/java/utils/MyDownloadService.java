package utils;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.greenrobot.event.EventBus;
import logcat.CLog;

/**
 * Created by sang on 16/03/2015.
 */
public class MyDownloadService extends IntentService {


    public static final String TAG = MyDownloadService.class.getSimpleName();
    public static final String KEY_CONTENT_TITLE = "content_title";
    public static final String KEY_DOWNLOAD_URL = "download_url";
    public static final String KEY_RECEIVER = "receiver";
    public static final String KEY_PROGRESS = "progress";
    public static final String KEY_CONTENT_ID = "content_id";
    public static final String KEY_FILE_PATH_DOWNLOAD = "file_url";
    public static final int UPDATE_RESULT_CODE = 2;
    public static final int FINISH_RESULT_CODE = 3;
    public static final int FAIL_RESULT_CODE = 4;
    public static final String KEY_CANCEL_DOWNLOAD = "cancel";
    private static final int VGAME_NOTIFICATION_ID = 1;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder mBuilder;
    private ResultReceiver receiver;
    private int contentId;
    private boolean isCancel;
    private String file_path;
    private String contentTitle;


    public MyDownloadService() {
        super(TAG);
    }


    public void publishProgress(int progress) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_PROGRESS, progress);
        bundle.putInt(KEY_CONTENT_ID, contentId);
        receiver.send(UPDATE_RESULT_CODE, bundle);
        mBuilder.setProgress(100, progress, false);
        mBuilder.setContentText("Downloaded " + progress + " %");
        notificationManager.notify(VGAME_NOTIFICATION_ID, mBuilder.build());
//        EventBus.getDefault().post(new UpdateDownloadEvent(progress, contentId));
    }

    public void onPreExecute() {
        mBuilder.setContentTitle(contentTitle);
        mBuilder.setProgress(100, 0, false);
        notificationManager.notify(VGAME_NOTIFICATION_ID, mBuilder.build());
    }

    public void onFinishDownload() {
        if (!isCancel) {
            mBuilder.setContentText("Download complete");
            mBuilder.setProgress(0, 0, false);
            notificationManager.notify(VGAME_NOTIFICATION_ID, mBuilder.build());
//            EventBus.getDefault().post(new UpdateDownloadEvent(100, contentId));
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(file_path)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else
            notificationManager.cancel(VGAME_NOTIFICATION_ID);


    }


    private void updateAPKFile(String link) {
        InputStream inputStream = null;
        try {
            //open conection
            URL url = new URL(link);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.connect();

            String fileApkName = link.hashCode() + ".apk";
            String PATH = Environment.getExternalStorageDirectory().getPath() + "/Download/";
            File filePath = new File(PATH);
            filePath.mkdirs();
            CLog.d(TAG, "Download path:" + PATH);
            File file = new File(PATH, fileApkName);
//                CLog.d(TAG, "file path:" + mFragmentActivity.getFilesDir() + File.separator + fileApkName);
            if (file.exists()) file.delete();
            FileOutputStream fileOutput = new FileOutputStream(file);
            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 200)
                inputStream = urlConnection.getInputStream();
            long fileLength = urlConnection.getContentLength();
            byte[] buffer = new byte[1024];
            long currentLength = 0;

            int bufferLength;
            int percentDone = -1;
            while (!isCancel && (bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
                currentLength += bufferLength;

                int progress = (int) ((currentLength * 100) / fileLength);
                if (fileLength > 0) {
                    if (percentDone != progress) {
                        percentDone = progress;
                        publishProgress(progress);
                    }
                }
            }

            inputStream.close();
            fileOutput.close();
            urlConnection.disconnect();
            Bundle resultFinish = new Bundle();
            file_path = file.getPath();
            resultFinish.putString(KEY_FILE_PATH_DOWNLOAD, file.getPath());
            resultFinish.putBoolean(KEY_CANCEL_DOWNLOAD, isCancel);
            receiver.send(FINISH_RESULT_CODE, resultFinish);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            receiver.send(FAIL_RESULT_CODE, null);
        } catch (IOException e) {
            e.printStackTrace();
            receiver.send(FAIL_RESULT_CODE, null);
        }


    }


    @Override
    protected void onHandleIntent(Intent intent) {
        receiver = (ResultReceiver) intent.getParcelableExtra(KEY_RECEIVER);
        String downloadUrl = intent.getStringExtra(KEY_DOWNLOAD_URL);
        contentTitle = intent.getStringExtra(KEY_CONTENT_TITLE);
        contentId = intent.getIntExtra(KEY_CONTENT_ID, -1);
        if (contentId != -1) {
            onPreExecute();
            updateAPKFile(downloadUrl);
            onFinishDownload();
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return super.onBind(intent);
    }

    @Override
    public void onDestroy() {
//        super.onDestroy();
        CLog.d(TAG, "on destroy");
        if (notificationManager != null)
            notificationManager.cancel(VGAME_NOTIFICATION_ID);
        isCancel = true;
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_CANCEL_DOWNLOAD, isCancel);
        bundle.putInt(KEY_CONTENT_ID, contentId);
        receiver.send(FAIL_RESULT_CODE, bundle);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        isCancel = false;
        mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setContentTitle("Downloading").setContentText("Download in progress").setSmallIcon(android.R.drawable.stat_sys_download);
    }
}
