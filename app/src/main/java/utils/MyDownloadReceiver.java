package utils;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.voalearningenglish.net.R;
import logcat.CLog;

/**
 * Created by sang on 16/03/2015.
 */
public class MyDownloadReceiver extends ResultReceiver {


    private static final String TAG = MyDownloadReceiver.class.getSimpleName();
    private Context context;

    public MyDownloadReceiver(Handler handler) {
        super(handler);
    }

    public MyDownloadReceiver(Context ctx, Handler handler) {
        super(handler);
        this.context = ctx;
    }


    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        CLog.d(TAG, "result code:" + resultCode);
        int contentId = resultData.getInt(MyDownloadService.KEY_CONTENT_ID);
        if (resultCode == MyDownloadService.UPDATE_RESULT_CODE) {
            int progress = resultData.getInt(MyDownloadService.KEY_PROGRESS);
//            EventBus.getDefault().post(new UpdateDownloadEvent(progress, contentId));
        } else if (resultCode == MyDownloadService.FAIL_RESULT_CODE) {
            //handler download fail
            if (resultData != null) {
                //handler cancel download
//                    Toast.makeText(mFragmentActivity, getString(R.string.cancel_download), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(context, context.getString(R.string.download_fail), Toast.LENGTH_SHORT).show();
//            EventBus.getDefault().post(new UpdateDownloadEvent(-1, contentId));
//                linearDownload.setVisibility(View.GONE);
//                linearOptionButton.setVisibility(View.VISIBLE);
        } else if (resultCode == MyDownloadService.FINISH_RESULT_CODE) {
            //handler install app
            boolean isCancel = resultData.getBoolean(MyDownloadService.KEY_CANCEL_DOWNLOAD);
            if (!isCancel) {
//                EventBus.getDefault().post(new UpdateDownloadEvent(100, contentId));
            }
        }

    }
}
