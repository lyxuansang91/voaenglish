package utils;

/**
 * Created by HoangAnh on 09/06/2015.
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

import logcat.CLog;
import models.RSSFeed;

public class HandleRSS {


    private static final String TAG = HandleRSS.class.getSimpleName();

    public static List<RSSFeed> getRssFeedsFromResponse(String response) {
        List<RSSFeed> list = new ArrayList<RSSFeed>();
        try {
            Document document = Jsoup.parse(response);
            List<Element> itemElements = document.select("item");
            CLog.d(TAG, "item element size:" + itemElements.size());
            for(int i = 0; i < itemElements.size(); i++) {
                Element item = itemElements.get(i);
                RSSFeed feed = new RSSFeed();
                feed.title = item.select("title").get(0).text();
                CLog.d(TAG, "title:" + feed.title);
                feed.description = item.select("description").get(0).text();
                CLog.d(TAG, "description:" + feed.description);
                feed.pubDate = item.select("pubDate").get(0).text();
                CLog.d(TAG, "pubDate:" + feed.pubDate);
                feed.link = item.select("guid").get(0).text();
                CLog.d(TAG, "link:" + feed.link);
                feed.urlImage = item.select("enclosure").attr("url");
                CLog.d(TAG, "url image:" + feed.urlImage);
                list.add(feed);
            }
            return list;
        }catch(Exception ex) {
            ex.printStackTrace();
            return new ArrayList<RSSFeed>();
        }


    }

    public List<RSSFeed> getRSSFeeds(String urlString) {
        List<RSSFeed> list;
        XmlPullParser myParser;
        XmlPullParserFactory xmlFactoryObject;
        InputStream stream = null;
        HttpURLConnection conn;
        URL url;

        Log.d("RSSHandler", "" + urlString);
        list = new ArrayList<RSSFeed>();
        // fetch xml
//        stream = fetchXML(url);
        try {
            url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            stream = conn.getInputStream();
            if(stream == null){
                Log.d("RSSHandler", "NULL");
            }
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            myParser = xmlFactoryObject.newPullParser();
            myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            myParser.setInput(stream, null);
            myParser.nextTag();
            parserXML(myParser, list);

        } catch (XmlPullParserException e) {
            list = new ArrayList<>();
            e.printStackTrace();
            Log.d("RSSHandler", "1:" + e.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("RSSHandler", "2:" + e.toString());
        } catch (ProtocolException e) {
            e.printStackTrace();
            Log.d("RSSHandler", "3:" + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("RSSHandler", "0:" + e.toString());
            }
        }
        return list;
    }

    private void parserXML(XmlPullParser myParser, List<RSSFeed> list) throws XmlPullParserException, IOException {
        int event;

        event = myParser.getEventType();
        RSSFeed feed = new RSSFeed();

        while (event != XmlPullParser.END_DOCUMENT) {

            String name = myParser.getName();
            boolean flag = true;
            if(flag){
                flag= false;
                feed = new RSSFeed();
                Log.d("RSSHandler", "new rss 2");
            }

            switch (event) {
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:

                    if(name.equals(RSSFeed.TAG_ITEM)){
                        feed = new RSSFeed();
                        Log.d("RSSHandler", "new rss");
                    }
                    switch (name) {
                        case RSSFeed.TAG_TITLE:
                            feed.title = myParser.nextText();
                            break;
                        case RSSFeed.TAG_GUID:
                            break;
                        case RSSFeed.TAG_LINK:
                            feed.link = myParser.nextText();
                            break;
                        case RSSFeed.TAG_PUBDATE:
                            feed.pubDate = myParser.nextText();
                            break;
                        case RSSFeed.TAG_ENCLOSURE:
                            feed.urlImage = myParser.getAttributeValue(null, "url");
//                            feed.title = myParser.getAttributeValue(null, "title");
//                            feed.pubDate = myParser.getAttributeValue(null, "pubDate");
//                            feed.link = myParser.getAttributeValue(null, "link");
                            list.add(feed);
                            Log.d("RSSHandler", "title: "+feed.title);
                            Log.d("RSSHandler", "pub date: "+feed.pubDate);
                            Log.d("RSSHandler", "link: "+feed.link);
                            Log.d("RSSHandler", "added rss");
                            flag = true;
                            break;
                        case RSSFeed.TAG_CATEGORY:
                            break;
                        case RSSFeed.TAG_COMMENTS:
                            break;
                        case RSSFeed.TAG_DESCRIPTION:
                            feed.description = myParser.nextText();
                            break;
                    }
                    break;
//                case XmlPullParser.TEXT:
//                    break;
//
//                case XmlPullParser.END_TAG:
//                    break;
            }
            event = myParser.next();
        }
    }


    public InputStream fetchXML(String urlString) {
        InputStream stream = null;
        HttpURLConnection conn;
        URL url;
        try {
            url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            stream = conn.getInputStream();

        } catch (Exception e) {

        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stream;
    }


}