package utils;

/**
 * Created by Admin on 17/4/2015.
 */
public class Define {
    public static final String KEY_SEND_CHANEL_TYPE = "ChanelType";
    public static final String KEY_SEND_PLAYLIST_ID = "PlaylistID";
    public static final String KEY_SEND_VIDEO_ID = "VideoID";
    public static final String KEY_SEND_VIDEO_IMAGE = "VideoImage";


    public static final String KEY_VIDEO = "VIDEO";
    public static final String KEY_PLAYLIST = "PLAYLIST";


    public static final String KEY_SEND_VIDEO_NAME = "VideoName";

    public static final String KEY_SEARCH = "searchKey";
    public static final String KEY_URL = "urlKey";
    public static final String KEY_TITLE = "title";
}
