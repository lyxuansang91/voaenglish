
package result.playlistinfo;

public class Thumbnails {
    private Default aDefault;
    private High high;
    private Medium medium;

    public Default getDefault() {
        return this.aDefault ;
    }

    public void setDefault(Default aDefault) {
        this.aDefault = aDefault;
    }

    public High getHigh() {
        return this.high;
    }

    public void setHigh(High high) {
        this.high = high;
    }

    public Medium getMedium() {
        return this.medium;
    }

    public void setMedium(Medium medium) {
        this.medium = medium;
    }
}
