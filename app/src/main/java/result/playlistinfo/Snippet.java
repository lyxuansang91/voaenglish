
package result.playlistinfo;

import java.util.List;

public class Snippet{
   	private String channelId;
   	private String channelTitle;
   	private String description;
   	private Localized localized;
   	private String publishedAt;
   	private List<String> tags;
   	private Thumbnails thumbnails;
   	private String title;

 	public String getChannelId(){
		return this.channelId;
	}
	public void setChannelId(String channelId){
		this.channelId = channelId;
	}
 	public String getChannelTitle(){
		return this.channelTitle;
	}
	public void setChannelTitle(String channelTitle){
		this.channelTitle = channelTitle;
	}
 	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
 	public Localized getLocalized(){
		return this.localized;
	}
	public void setLocalized(Localized localized){
		this.localized = localized;
	}
 	public String getPublishedAt(){
		return this.publishedAt;
	}
	public void setPublishedAt(String publishedAt){
		this.publishedAt = publishedAt;
	}
 	public List<String> getTags(){
		return this.tags;
	}
	public void setTags(List<String> tags){
		this.tags = tags;
	}
 	public Thumbnails getThumbnails(){
		return this.thumbnails;
	}
	public void setThumbnails(Thumbnails thumbnails){
		this.thumbnails = thumbnails;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
}
