
package result.playlistinfo;

import java.util.List;

public class PlayListInfo{
   	private String etag;
   	private List<Items> items;
   	private String kind;
   	private PageInfo pageInfo;

 	public String getEtag(){
		return this.etag;
	}
	public void setEtag(String etag){
		this.etag = etag;
	}
 	public List<Items> getItems(){
		return this.items;
	}
	public void setItems(List<Items> items){
		this.items = items;
	}
 	public String getKind(){
		return this.kind;
	}
	public void setKind(String kind){
		this.kind = kind;
	}
 	public PageInfo getPageInfo(){
		return this.pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo){
		this.pageInfo = pageInfo;
	}
}
