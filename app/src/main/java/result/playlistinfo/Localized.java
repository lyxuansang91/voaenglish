
package result.playlistinfo;

import java.util.List;

public class Localized{
   	private String description;
   	private String title;

 	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
}
