
package result.playlistitem;

import java.util.List;

public class PageInfo{
   	private Number resultsPerPage;
   	private Number totalResults;

 	public Number getResultsPerPage(){
		return this.resultsPerPage;
	}
	public void setResultsPerPage(Number resultsPerPage){
		this.resultsPerPage = resultsPerPage;
	}
 	public Number getTotalResults(){
		return this.totalResults;
	}
	public void setTotalResults(Number totalResults){
		this.totalResults = totalResults;
	}
}
