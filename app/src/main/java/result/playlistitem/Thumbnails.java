
package result.playlistitem;

public class Thumbnails{
   	private Default aDefault;
   	private High high;
   	private Maxres maxres;
   	private Medium medium;
   	private Standard standard;

 	public Default getDefault(){
		return this.aDefault;
	}
	public void setDefault(Default aDefault){
		this.aDefault = aDefault;
	}
 	public High getHigh(){
		return this.high;
	}
	public void setHigh(High high){
		this.high = high;
	}
 	public Maxres getMaxres(){
		return this.maxres;
	}
	public void setMaxres(Maxres maxres){
		this.maxres = maxres;
	}
 	public Medium getMedium(){
		return this.medium;
	}
	public void setMedium(Medium medium){
		this.medium = medium;
	}
 	public Standard getStandard(){
		return this.standard;
	}
	public void setStandard(Standard standard){
		this.standard = standard;
	}
}
