
package result.playlistitem;

import java.util.List;

public class Snippet{
   	private String channelId;
   	private String channelTitle;
   	private String description;
   	private String playlistId;
   	private Number position;
   	private String publishedAt;
   	private ResourceId resourceId;
   	private Thumbnails thumbnails;
   	private String title;

 	public String getChannelId(){
		return this.channelId;
	}
	public void setChannelId(String channelId){
		this.channelId = channelId;
	}
 	public String getChannelTitle(){
		return this.channelTitle;
	}
	public void setChannelTitle(String channelTitle){
		this.channelTitle = channelTitle;
	}
 	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
 	public String getPlaylistId(){
		return this.playlistId;
	}
	public void setPlaylistId(String playlistId){
		this.playlistId = playlistId;
	}
 	public Number getPosition(){
		return this.position;
	}
	public void setPosition(Number position){
		this.position = position;
	}
 	public String getPublishedAt(){
		return this.publishedAt;
	}
	public void setPublishedAt(String publishedAt){
		this.publishedAt = publishedAt;
	}
 	public ResourceId getResourceId(){
		return this.resourceId;
	}
	public void setResourceId(ResourceId resourceId){
		this.resourceId = resourceId;
	}
 	public Thumbnails getThumbnails(){
		return this.thumbnails;
	}
	public void setThumbnails(Thumbnails thumbnails){
		this.thumbnails = thumbnails;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
}
