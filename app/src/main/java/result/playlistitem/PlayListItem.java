
package result.playlistitem;

import java.util.ArrayList;
import java.util.List;

public class PlayListItem{
   	private String etag;
   	private ArrayList<Items> items;
   	private String kind;
   	private String nextPageToken;
   	private PageInfo pageInfo;


 	public String getEtag(){
		return this.etag;
	}
	public void setEtag(String etag){
		this.etag = etag;
	}
 	public ArrayList<Items> getItems(){
		return this.items;
	}
	public void setItems(ArrayList<Items> items){
		this.items = items;
	}
 	public String getKind(){
		return this.kind;
	}
	public void setKind(String kind){
		this.kind = kind;
	}
 	public String getNextPageToken(){
		return this.nextPageToken;
	}
	public void setNextPageToken(String nextPageToken){
		this.nextPageToken = nextPageToken;
	}
 	public PageInfo getPageInfo(){
		return this.pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo){
		this.pageInfo = pageInfo;
	}
}
