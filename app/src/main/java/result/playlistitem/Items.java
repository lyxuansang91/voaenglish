
package result.playlistitem;

public class Items {

   	private ContentDetails contentDetails;
   	private String etag;
   	private String id;
   	private String kind;
   	private Snippet snippet;

 	public ContentDetails getContentDetails(){
		return this.contentDetails;
	}
	public void setContentDetails(ContentDetails contentDetails){
		this.contentDetails = contentDetails;
	}
 	public String getEtag(){
		return this.etag;
	}
	public void setEtag(String etag){
		this.etag = etag;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getKind(){
		return this.kind;
	}
	public void setKind(String kind){
		this.kind = kind;
	}
 	public Snippet getSnippet(){
		return this.snippet;
	}
	public void setSnippet(Snippet snippet){
		this.snippet = snippet;
	}
}
