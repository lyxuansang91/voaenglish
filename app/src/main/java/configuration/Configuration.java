package configuration;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * Created by sang on 29/01/2015.
 */
public class Configuration {

    public static final boolean DEBUG = true;
    public static final int APPLICATION_TYPE = 1;
    public static final int GAME_TYPE = 2;
    public static final int BOOK_TYPE = 3;
    public static final int COMIC_TYPE = 4;
    public static final int MANAGER_FRIEND_TYPE = 9;


    public static final int ANDROID_PLATFORM = 1;
    public static final int ADS_DELAY = 5000;

    public static final int REQUIRE_LOGIN_CODE = 401;

    public static final int NUMBER_MORE = 50;
    public static final int ITEM_PER_PAGE = 10;
    public static final String Pref = "myPref";
    public static final String SHARE_CONTENT = "https://play.google.com/store/apps/details?id=com.voalearningenglish.net";
    public static final String ABOUT_US_LINK = "http://www.voalearningenglish.net/";
    public static final String FAN_PAGE = "https://www.facebook.com/LearnEnglishConversation";
    public static final String OTHER_APP = "https://play.google.com/store/apps/developer?id=Learn+English+247";
    public static final String RATING = "https://play.google.com/store/apps/details?id=com.voalearningenglish.net";


    public static final String ID_ADS = "ca-app-pub-8383112288709807/5027058772";


    public static boolean isReadyForPullEnd(RecyclerView recyclerView) {
        View lastView = recyclerView.getChildAt(recyclerView.getChildCount() - 1);
        int lastPosition = recyclerView.getChildPosition(lastView);

        if (lastPosition >= recyclerView.getAdapter().getItemCount() - 1) {
            return recyclerView.getChildAt(recyclerView.getChildCount() - 1).getBottom() <= recyclerView.getBottom();
        }
        return false;
    }

    public static String decodeString(String strEncode) {

        try {
            if (strEncode == null)
                return null;
            return URLDecoder.decode(strEncode, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean appInstallOrNot(Context ctx, String uri) {
        PackageManager pm = ctx.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static PackageInfo getPackageInfo(Context ctx, String uri) {
        try {
            return ctx.getPackageManager().getPackageInfo(uri, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
