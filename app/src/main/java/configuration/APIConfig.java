package configuration;

/**
 * Created by sang on 29/01/2015.
 */
public class APIConfig {

    public static final String API_KEY = "AIzaSyBGYjXs9XO7TBsA2fi8E8W88SFVggah5Fs";

    public static final String HOST = "https://www.googleapis.com";

    public static final String ANDROID_API_KEY = "AIzaSyCQz3BSnVr0_RBUYEn5ai95ORcZyAprvY8";

    // api
    public static final String PLAYLIST_INFO = "https://www.googleapis.com/youtube/v3/playlists?key=%s&id=%s&part=id,contentDetails,snippet";
    public static final String PLAYLIST_VIDEOS_LIST = "https://www.googleapis.com/youtube/v3/playlistItems?key=%s&playlistId=%s&part=id,snippet,contentDetails&pageToken=%s";
    public static final String CHANNEL_VIDEOS_LIST = "https://www.googleapis.com/youtube/v3/search?key=%s&chanelId=%s&maxResults=20&part=snippet,id&order=date";

}
