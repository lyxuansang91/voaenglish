package com.androidteam.englishlearning.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;
import com.startapp.android.publish.video.VideoListener;
import com.voalearningenglish.net.R;

/**
 * Created by Admin on 20/4/2015.
 */
public class HomeScreenActivity extends ActionBarActivity {

    private final String TAG = this.getClass().getSimpleName();

    private final int DELAY_TIME = 300;
    private ProgressBar progressBar;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, getString(R.string.start_app_id), true);
        setContentView(R.layout.activity_home_screen);
        this.setTitle("Splash screen");
        init();
    }

    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }

    private void init() {
        final InterstitialAd mInterstitialAd;
        //bind view
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        progressBar.setVisibility(View.VISIBLE);
        startAppAd.loadAd(StartAppAd.AdMode.REWARDED_VIDEO);
        startAppAd.setVideoListener(new VideoListener() {
            @Override
            public void onVideoCompleted() {
                // Grant user with the reward
                new Handler().postDelayed(mUpdateTimeTask, DELAY_TIME);
            }
        });

    }



    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            progressBar.setVisibility(View.GONE);
            startActivity(new Intent(HomeScreenActivity.this,MainActivity.class));
            finish();
        }
    };

}
