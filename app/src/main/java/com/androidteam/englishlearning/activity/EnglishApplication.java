package com.androidteam.englishlearning.activity;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;

import com.voalearningenglish.net.R;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import configuration.APIConfig;
import configuration.Configuration;
import network.ApiService;
import network.PersistentCookieStore;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


/**
 * Created by sang on 02/02/2015.
 */
public class EnglishApplication extends Application {


    private static EnglishApplication _instance = null;

    public int currentPosition = 1;
    public int probality = 0;
    public View footerView;
    public SharedPreferences sharedPreferences;

    public Retrofit retrofit;
    public static OkHttpClient okHttpClient;
    public ApiService apiService;

    public static EnglishApplication getInstance() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footerView = layoutInflater.inflate(R.layout.footer, null);
        sharedPreferences = getSharedPreferences(Configuration.Pref, Context.MODE_PRIVATE);
        initRetrofit();
        apiService = retrofit.create(ApiService.class);
    }

    private void initRetrofit() {
        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(this), CookiePolicy.ACCEPT_ALL);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        if (this.okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder()
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .cookieJar(new JavaNetCookieJar(cookieManager))
                    .addInterceptor(logging)
                    .build();
        }

        retrofit = new Retrofit.Builder()
//                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(APIConfig.HOST)
                .client(okHttpClient)
                .build();
    }


}
