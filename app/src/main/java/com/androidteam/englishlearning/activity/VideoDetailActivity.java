package com.androidteam.englishlearning.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.voalearningenglish.net.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import configuration.APIConfig;
import configuration.Configuration;
import logcat.CLog;
import models.Note;
import utils.Define;
import utils.NotesDatabaseHandler;

public class VideoDetailActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = VideoDetailActivity.class.getSimpleName();
    private YouTubePlayerSupportFragment youTubePlayerFragment;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private String videoId;
    private String videoName;
    private String videoImage;

    private YouTubePlayer youTubePlayer;
    private EditText edtNote;
    private Button btnSaveNote;
    private Button btnNote;
    private Button btnCancelNote;
    private ScrollView svNote;

    private void setupFragment(FragmentManager fm, int resId, Fragment fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(resId, fragment);
        ft.commit();
    }


    private void showNote(boolean isOpen) {
        btnNote.setVisibility(isOpen ? View.GONE : View.VISIBLE);
        svNote.setVisibility(isOpen ? View.VISIBLE : View.GONE);
    }

    private void saveNote(String videoId) {
        //TODO: save note here
        NotesDatabaseHandler db = new NotesDatabaseHandler(VideoDetailActivity.this);
        Note note = db.getNoteByID(videoId);
        String note_content = edtNote.getText().toString();
        Note newNote = new Note(videoId, videoName, note_content, videoImage);
        if (note != null) {
            boolean isUpdated = db.updateNote(newNote);
            Toast.makeText(VideoDetailActivity.this, isUpdated ? getString(R.string.updated_success) : getString(R.string.updated_failed), Toast.LENGTH_SHORT).show();
            if (isUpdated) btnNote.setText(note_content);
        } else {
            boolean isAdded = db.addNote(newNote);
            Toast.makeText(VideoDetailActivity.this, isAdded ? getString(R.string.updated_success) : getString(R.string.updated_failed), Toast.LENGTH_SHORT).show();
            if (isAdded) btnNote.setText(note_content);
        }

    }

    private void initUI() {
        btnNote = (Button) findViewById(R.id.btnNote);
        btnSaveNote = (Button) findViewById(R.id.btnSaveNote);
        btnCancelNote = (Button) findViewById(R.id.btnCancelNote);
        svNote = (ScrollView) findViewById(R.id.svNote);
        edtNote = (EditText) findViewById(R.id.tvNote);
        btnNote.setOnClickListener(this);
        btnSaveNote.setOnClickListener(this);
        btnCancelNote.setOnClickListener(this);
        showNote(false);
        CLog.d(TAG, "videoId:" + videoId);
        youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        if (youTubePlayerFragment != null) {
            CLog.d(TAG, "on success load fragment");
            setupFragment(getSupportFragmentManager(), R.id.youtube_fragment, youTubePlayerFragment);
        }

        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
                CLog.d(TAG, "(On success)");
                VideoDetailActivity.this.youTubePlayer = youTubePlayer;
                VideoDetailActivity.this.youTubePlayer.setShowFullscreenButton(true);
                VideoDetailActivity.this.youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                if (!wasRestored) {
                    if (videoId != null)
                        VideoDetailActivity.this.youTubePlayer.loadVideo(videoId);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
                CLog.d(TAG, "(On failed)");
                if (errorReason.isUserRecoverableError()) {
                    errorReason.getErrorDialog(VideoDetailActivity.this, RECOVERY_DIALOG_REQUEST).show();
                } else {
                    Toast.makeText(VideoDetailActivity.this, errorReason.toString(), Toast.LENGTH_LONG).show();
                }
            }
        };

        youTubePlayerFragment.initialize(APIConfig.ANDROID_API_KEY, onInitializedListener);

    }

    private Note getNoteFromId(String video_id) {
        NotesDatabaseHandler db = new NotesDatabaseHandler(VideoDetailActivity.this);
        return db.getNoteByID(video_id);
    }

    private void initData() {
        CLog.d(TAG, "init data");


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            videoId = bundle.getString(Define.KEY_SEND_VIDEO_ID, "");
            videoName = bundle.getString(Define.KEY_SEND_VIDEO_NAME, "");
            videoImage = bundle.getString(Define.KEY_SEND_VIDEO_IMAGE, "");
            setTitleActionBar(videoName);
        }

        Note note = getNoteFromId(videoId);
        btnNote.setText(note == null ? getResources().getString(R.string.note) : note.getNote_content());
        //btnNote.setText();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getYouTubePlayerProvider().initialize(APIConfig.ANDROID_API_KEY, onInitializedListener);
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubePlayerFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //fix padding
        RelativeLayout layoutNote = (RelativeLayout) findViewById(R.id.layout_note);
        layoutNote.setPadding(0, 0, 0, 0);
        initUI();

        initData();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_video_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.share) {
            // Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_option), Toast.LENGTH_SHORT).show();
            CLog.d(TAG, "share intent");
            Intent shareintent = new Intent();
            shareintent.setAction(Intent.ACTION_SEND);
            shareintent.putExtra(Intent.EXTRA_TEXT, Configuration.SHARE_CONTENT);
            shareintent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.default_share_content));
            shareintent.setType("text/plain");
            startActivity(Intent.createChooser(shareintent, Configuration.SHARE_CONTENT));
            return true;
        }
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNote:
                showNote(true);
                break;
            case R.id.btnCancelNote:
                showNote(false);
                break;
            case R.id.btnSaveNote:
                saveNote(videoId);
                break;
        }
    }
}
