package com.androidteam.englishlearning.activity;

import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;
import com.startapp.android.publish.video.VideoListener;
import com.voalearningenglish.net.R;

import configuration.Configuration;
import de.greenrobot.event.EventBus;
import event.ChanelItemClickEvent;
import event.ChannelItemClickEvent;
import event.DrawListPositionChangedEvent;
import event.DrawerListItemClickEvent;
import event.PlaylistItemClickEvent;
import event.RSSFeedTypeClickEvent;
import event.VideoItemClickEvent;
import fragment.ChanelListFragment;
import fragment.FavoritePlaylistListFragment;
import fragment.HomeFragment;
import fragment.NavigationDrawerFragment;
import fragment.NotesFragment;
import fragment.PlaylistListFragment;
import fragment.PlaylistSearchFragment;
import fragment.RSSFeedFragment;
import fragment.VideoListFragment;
import logcat.CLog;
import models.CategoryDrawerListItem;
import models.DrawerListItem;
import models.EChanelType;
import models.MenuDrawerListItem;
import result.playlistitem.Items;
import utils.Define;


public class MainActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    private static final int VGAME_NOTIFICATION_ID = 1;
    private static final String KEY_POSITION = "position";
    private static final String KEY_MY_SEFL = "ismysefl";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String CATEGORY_TAG = ChanelListFragment.class.getSimpleName();
    private static final String HOME_TAG = HomeFragment.class.getSimpleName();
    private static long back_pressed;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager notificationManager;
    private Handler handler;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private Class<? extends Fragment> currentFragmentClass = null;
    private FragmentManager fragmentManager;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        StartAppSDK.init(this, getString(R.string.start_app_id), true);
//        StartAppAd.showSplash(this, savedInstanceState);

        setContentView(R.layout.activity_main);
        initVideoAds();
        handler = new Handler();
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fr = fragmentManager.findFragmentById(R.id.container);
                if (fr != null) {
                    currentFragmentClass = fr.getClass();
                    Bundle bundle = fr.getArguments();

                    if (currentFragmentClass == HomeFragment.class) {
                        mTitle = getString(R.string.homepage);
                    }

                    if (currentFragmentClass == NotesFragment.class) {
                        mTitle = getString(R.string.notes);
                    }

                    if (currentFragmentClass == PlaylistListFragment.class) {
                        mTitle = bundle.getSerializable(Define.KEY_SEND_CHANEL_TYPE).toString();
                    }

                    if (currentFragmentClass == VideoListFragment.class) {
                        mTitle = getString(R.string.app_name);
                    }


                    if (currentFragmentClass == FavoritePlaylistListFragment.class) {
                        mTitle = getString(R.string.favorite);
                    }

                    if (currentFragmentClass == RSSFeedFragment.class) {
                        mTitle = getString(R.string.app_name);
                    }

                    CLog.d(TAG, "current title:" + mTitle);
                    if (bundle.getInt(MainActivity.KEY_POSITION) != 0)
                        EnglishApplication.getInstance().currentPosition = bundle.getInt(MainActivity.KEY_POSITION);
                    restoreActionBar();
                    EventBus.getDefault().post(new DrawListPositionChangedEvent());
                }
                CLog.d(TAG, "current fragment class:" + currentFragmentClass);
            }
        });
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        goHome();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }

    private void initVideoAds() {
//        startAppAd.showAd();
//        startAppAd.loadAd(StartAppAd.AdMode.REWARDED_VIDEO);
//        startAppAd.setVideoListener(new VideoListener() {
//            @Override
//            public void onVideoCompleted() {
//            }
//        });

        //ads
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice("3D4AA0D7B032A447E941A81B32607341")
                .build();
        mAdView.loadAd(adRequest);

        new Handler().postDelayed(mLoadAds, 10000);
    }

    private InterstitialAd mInterstitialAd;

    private Runnable mLoadAds = new Runnable() {
        public void run() {

            mInterstitialAd = new InterstitialAd(MainActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                }

                @Override
                public void onAdLoaded() {

                    mInterstitialAd.show();
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    new Handler().postDelayed(mLoadAds, 10000);
                }
            });

            requestNewInterstitial(mInterstitialAd);
        }
    };

    private void requestNewInterstitial(InterstitialAd mInterstitialAd) {
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice("3D4AA0D7B032A447E941A81B32607341")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    public void onEvent(ChannelItemClickEvent event) {
        CategoryDrawerListItem categoryDrawerListItem = event.categoryDrawerListItem;
        addFragment(VideoListFragment.getNewInstance(categoryDrawerListItem.getCategoryId()), VideoListFragment.class.getSimpleName());
    }

    public void onEvent(PlaylistItemClickEvent event) {
        CLog.d(TAG, "(on playlist click item)");
        String playListId = event.playListId;
        Bundle bundle = new Bundle();
        bundle.putString(Define.KEY_SEND_PLAYLIST_ID, playListId);
        addFragment(VideoListFragment.getNewInstance(playListId), VideoListFragment.class.getSimpleName(), true, 3);
    }

    public void onEvent(VideoItemClickEvent event) {
        CLog.d(TAG, "(on video click item)");
        Intent intent = new Intent(getApplicationContext(), VideoDetailActivity.class);
        Items item = event.items;
        Bundle bundle = new Bundle();

//        bundle.putSerializable(Define.KEY_ITEM, event.items);
        bundle.putString(Define.KEY_SEND_VIDEO_ID, item.getContentDetails().getVideoId());
        bundle.putString(Define.KEY_SEND_VIDEO_NAME, item.getSnippet().getTitle());

        String url = "";

        if (item.getSnippet().getThumbnails().getDefault() != null) {
            url = item.getSnippet().getThumbnails().getDefault().getUrl();
        } else if (item.getSnippet().getThumbnails().getHigh() != null) {
            url = item.getSnippet().getThumbnails().getHigh().getUrl();
        } else if (item.getSnippet().getThumbnails().getMedium() != null) {
            url = item.getSnippet().getThumbnails().getMedium().getUrl();
        }
        bundle.putString(Define.KEY_SEND_VIDEO_IMAGE, url);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        if (mNavigationDrawerFragment.isDrawerOpen()) {
            ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawers();
            return;
        }

        if (currentFragmentClass != HomeFragment.class) {
            CLog.d(TAG, "on back pressed!" + currentFragmentClass);
            super.onBackPressed();
            return;
        }

        if (System.currentTimeMillis() - back_pressed < 2000) {

            finish();
        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.ClickBackAgainToExitTheApplication), Toast.LENGTH_SHORT).show();
        }

        back_pressed = System.currentTimeMillis();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    private void goHome() {
        CLog.d(TAG, "currentFragmentClass :" + currentFragmentClass);
        if (currentFragmentClass == HomeFragment.class) { // dang o Home roi, ko lam gi ca
            return;
        }

        // chua co fragment nao, add fragment home
        if (currentFragmentClass == null) {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right)
                    .addToBackStack(HOME_TAG)
                    .replace(R.id.container, HomeFragment.getNewInstance(1), HOME_TAG)
                    .commit();
        } else {
            // da add fragment khac de le Home, pop back stack cho den khi gap Home
            fragmentManager.popBackStack(HOME_TAG, 0);
        }

    }

    private void addFragment(Fragment fragment, String name) {
        addFragment(fragment, name, true, 1);
    }

    private void addFragment(Fragment fragment, String name, int level) {
        addFragment(fragment, name, true, level);
    }

    private void addFragment(Fragment fragment, String name, boolean animated, int level) {
        // neu ko phai dang o Home, thi pop back den Home


//        CLog.d(TAG, "fragment backstack count:" + fragmentManager.getBackStackEntryCount());
//        if (level == 0) {
//            fragmentManager.popBackStack(CHANNEL_LIST_TAG, 0);
//        }

        if (level == 1) {
            if (currentFragmentClass != HomeFragment.class) {
                fragmentManager.popBackStack(HOME_TAG, 0);
            }
        }


        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (animated) {
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
        }

        // add de giu nguyen fragment ben duoi, replace de thay the, khi pop back thi fragment ben duoi dc khoi tao lai
        ft.addToBackStack(name);
        ft.replace(R.id.container, fragment);
//        if (level == 1) {
//            ft.add(R.id.container, fragment);
//        } else {
//            ft.replace(R.id.container, fragment);
//        }
        CLog.d(TAG, "level:" + level + ", current fragment class:" + currentFragmentClass);
        ft.commit();
    }

    public void replaceFragment(Fragment fragment, String name) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment, name);
        ft.commit();
    }

    public void onEvent(ChanelItemClickEvent event) {
        CLog.d(TAG, "(Channel item click event)");
        EChanelType chanelType = event.chanelType;
        addFragment(PlaylistListFragment.getNewInstance(chanelType), PlaylistListFragment.class.getSimpleName(), true, 2);
    }

    public void onEvent(DrawerListItemClickEvent event) {
        DrawerListItem drawerListItem = event.mDrawerListItem;
        switch (drawerListItem.getType()) {
            case DrawerListItem.DRAWER_MENU_TYPE:
                if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_HOME) {
                    mTitle = getResources().getString(R.string.homepage);
                    goHome();
                } else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_NOTE) {
                    mTitle = getResources().getString(R.string.notes);
                    addFragment(NotesFragment.getnewInstance(event.position), NotesFragment.class.getSimpleName());
                } else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_FAVORITE_LIST) {
                    mTitle = getResources().getString(R.string.favorite);
                    addFragment(FavoritePlaylistListFragment.getNewInstance(event.position), FavoritePlaylistListFragment.class.getSimpleName());
                }

//                CLog.d(TAG, "menu id:" + ((MenuDrawerListItem) drawerListItem).getMenuId());

                else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_SHARE_APP) {
                    CLog.d(TAG, "share intent");
                    Intent shareintent = new Intent();
                    shareintent.setAction(Intent.ACTION_SEND);
                    shareintent.putExtra(Intent.EXTRA_TEXT, Configuration.SHARE_CONTENT);
                    shareintent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.default_share_content));
                    shareintent.setType("text/plain");
                    startActivity(Intent.createChooser(shareintent, Configuration.SHARE_CONTENT));
                } else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_FAN_PAGE) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.FAN_PAGE));
                    startActivity(browserIntent);
                } else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_RATING_FOR_US) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.RATING));
                    startActivity(browserIntent);
                } else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_OTHER_APP) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.OTHER_APP));
                    startActivity(browserIntent);
                } else if (((MenuDrawerListItem) drawerListItem).getMenuId() == DrawerListItem.ID_ABOUT) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Configuration.ABOUT_US_LINK));
                    startActivity(browserIntent);
                }
                break;

            case DrawerListItem.DRAWER_ACCOUNT_TYPE:
                break;
            case DrawerListItem.DRAWER_CATEGORY_TYPE:
                break;
        }
        EnglishApplication.getInstance().currentPosition = event.position;
        setTitleActionBar(mTitle.toString());
    }

    public void onEvent(RSSFeedTypeClickEvent event) {
        CLog.d(TAG, "(RSSFeedCategory click event)");
        String key = event.rssFeedURL.url;
//        addFragment(RSSFeedFragment.getNewInstance(key), RSSFeedFragment.class.getSimpleName(), true, 2);
        addFragment(RSSFeedFragment.getNewInstance(key), RSSFeedFragment.class.getSimpleName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        findViewById(R.id.adsContainer).setVisibility(View.VISIBLE);

    }

    private void doMySearch(String query) {

    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SearchManager searchManager;
        SearchView searchView;

//        getMenuInflater().inflate(R.menu.main, menu);
//        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView = (SearchView) menu.findItem(R.id.mnuSearch).getActionView();
//        if (null != searchView) {
//            searchView.setSearchableInfo(searchManager
//                    .getSearchableInfo(getComponentName()));
//            searchView.setIconifiedByDefault(false);
//        }


        // navigation drawer
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) menu.findItem(R.id.mnuSearch).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(this);
//            }


            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {

        addFragment(PlaylistSearchFragment.getNewInstance(s.toLowerCase().trim()), PlaylistSearchFragment.class.getSimpleName(), true, 1);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }


}
