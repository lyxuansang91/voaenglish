package com.androidteam.englishlearning.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.FrameLayout;

import com.voalearningenglish.net.R;

import de.greenrobot.event.EventBus;
import event.ChanelItemClickEvent;
import event.PlaylistItemClickEvent;
import event.VideoItemClickEvent;
import fragment.ChanelListFragment;
import fragment.PlaylistListFragment;
import fragment.VideoListFragment;
import models.EChanelType;
import utils.Define;

public class TestActivity extends BaseActivity {

    private static final String TAG = TestActivity.class.getSimpleName();

    @Override
    protected void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        this.setTitle("ENGLISH LEARNING");

        init();
    }

    private void init() {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, ChanelListFragment.getNewInstance(0)).addToBackStack(ChanelListFragment.class.getSimpleName()).commit();
    }

//    public void onEvent(Object ob) {
//        Log.d(TAG, "onEvent: " + ob.getClass());
//
//        if (ob instanceof ChanelItemClickEvent) {
//            EChanelType chanelType = ((ChanelItemClickEvent) ob).chanelType;
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(Define.KEY_SEND_CHANEL_TYPE, chanelType);
//            Fragment fragment = new PlaylistListFragment();
//            fragment.setArguments(bundle);
//
//            replaceFragment(fragment, "1 to 2");
//
//        } else if (ob instanceof PlaylistItemClickEvent) {
//            String playListId = ((PlaylistItemClickEvent) ob).playListId;
//            Bundle bundle = new Bundle();
//            bundle.putString(Define.KEY_SEND_PLAYLIST_ID, playListId);
//            Fragment fragment = new VideoListFragment();
//            fragment.setArguments(bundle);
//
//            replaceFragment(fragment, "2 to 3");
//
//        } else if (ob instanceof VideoItemClickEvent) {
//            String videoId = ((VideoItemClickEvent) ob).videoId;
//            Bundle bundle = new Bundle();
//            bundle.putString(Define.KEY_SEND_VIDEO_ID, videoId);
//
//            Fragment fragment = new VideoListFragment();
//            fragment.setArguments(bundle);
//
//            replaceFragment(fragment, "3 to 4");
//
//        }
//    }



    public void replaceFragment(Fragment newFragment, String tag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, newFragment, tag).addToBackStack(null).commit();
    }

}
