package com.androidteam.englishlearning.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.voalearningenglish.net.R;

import utils.Define;

/**
 * Created by AnhTH on 14/6/2015.
 */
public class FeedViewerActivity extends BaseActivity{
    final String TAG = this.getClass().getSimpleName();

    String url;
    String title;

    WebView webview;
    ProgressBar progressBar;

    @Override
    public void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_view_activity);
        init();
    }

    private void init() {
        final InterstitialAd mInterstitialAd;

        // binding views
        webview = (WebView) findViewById(R.id.webView);

        //back
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);


        //display ads
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }
        });


//        //fix padding
//        int height = mAdView.getHeight();
//        RelativeLayout layoutNote = (RelativeLayout) findViewById(R.id.layout_note);
//        layoutNote.setPadding(0, height, 0, 0);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        url = getIntent().getStringExtra(Define.KEY_URL);
        title = getIntent().getStringExtra(Define.KEY_TITLE);

        if(url == null)
            url = "http://www.google.com.vn/";

        this.setTitle(title);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setUserAgentString("Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3");
        webview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }
        });
        webview.loadUrl(url);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_video_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id==android.R.id.home) {
            // app icon in action bar clicked; go home
            finish();
            return true;
        }

        if (id == R.id.share) {
            Intent shareintent = new Intent();
            shareintent.setAction(Intent.ACTION_SEND);
            shareintent.putExtra(Intent.EXTRA_TEXT, url);
            shareintent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.default_share_content));
            shareintent.setType("text/plain");
            startActivity(Intent.createChooser(shareintent, url));
            return true;
        }
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void requestNewInterstitial(InterstitialAd mInterstitialAd) {
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice("00DF949DD754C2C7DEF2F51F706F0A86").build();
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }
}
