package event;

import models.CategoryDrawerListItem;

/**
 * Created by sang on 6/15/2015.
 */
public class ChannelItemClickEvent {
    public CategoryDrawerListItem categoryDrawerListItem;
    public ChannelItemClickEvent(CategoryDrawerListItem categoryDrawerListItem) {
        this.categoryDrawerListItem = categoryDrawerListItem;
    }
}
