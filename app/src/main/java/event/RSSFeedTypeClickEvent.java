package event;

import utils.RSSFeedURL;

/**
 * Created by HoangAnh on 10/06/2015.
 */
public class RSSFeedTypeClickEvent {

    public RSSFeedURL rssFeedURL;

    public RSSFeedTypeClickEvent(RSSFeedURL rssFeedURL) {
        this.rssFeedURL = rssFeedURL;
    }
}
