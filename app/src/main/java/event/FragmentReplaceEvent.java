package event;

import android.app.Fragment;

/**
 * Created by Admin on 17/4/2015.
 */
public class FragmentReplaceEvent {

    public Fragment fragment;

    public FragmentReplaceEvent(Fragment fragment) {
        this.fragment = fragment;
    }
}
