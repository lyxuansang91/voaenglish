package event;


import models.DrawerListItem;

/**
 * Created by sang on 02/02/2015.
 */
public class DrawerListItemClickEvent {
    public int position;
    public DrawerListItem mDrawerListItem;

    public DrawerListItemClickEvent(DrawerListItem _drawerListItem, int _position) {
        this.position = _position;
        this.mDrawerListItem = _drawerListItem;
    }
}
